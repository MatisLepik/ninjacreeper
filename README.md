### NinjaCreeper ###

This was one of my first Java projects. It took a good couple of months to make and taught me a lot about programming. I never fully completed it, so it's pretty barebones, but everything is functional. I'm no artist, so the visuals are very basic.

In short, this is a 2D platformer. The name (and some of the graphical assets) are "inspired" by Minecraft, because why not =P

There is also a level editor (NCLevelEdit.jar, open it in cmd "java -jar NCLevelEdit.jar").

**You can download an executable here:**
[Windows version](http://www.mediafire.com/download/uk0iilhkpru8i7j/NinjaCreeper.exe) | [OSX version](http://www.mediafire.com/download/bsbw0rzhp4zx7b3/NinjaCreeper-OSX.zip)

### Setup ###

Dependencies:

- LWJGL 2.9.1

- EasyOGG

- Slick2D

### Contact ###

- Matis Lepik

- matis.lepik@gmail.com

- www.matislepik.eu

![Ingame screenshots](https://i.imgur.com/fCAbemC.png)
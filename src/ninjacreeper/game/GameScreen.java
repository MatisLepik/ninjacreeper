package ninjacreeper.game;

import java.awt.geom.Point2D;
import java.nio.DoubleBuffer;
import java.util.ArrayList;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.opengl.Texture;

/**
 *
 * @author Matis
 */
public class GameScreen extends GameState {

    public static Level level;
    public static Player player;
    public static Finish finish;
    public static ArrayList<Enemy> enemies;
    public static ArrayList<Turret> turrets;
    public static ArrayList<Gunpowder> gunpowders;
    //public static double timeScale = 0.25;

    public GameScreen() {

    }

    public static void setUpGame(double playerX, double playerY, double finishX, double finishY, ArrayList<Point2D> enemyPositions, ArrayList<Point2D> gunpowderPositions, ArrayList<double[]> turretData, char[][] blockData, Texture backgroundImage, Texture foregroundImage, int levelId) {
        level = new Level(backgroundImage, foregroundImage, blockData, levelId);
        player = new Player(playerX, playerY);
        finish = new Finish(finishX, finishY);
        enemies = new ArrayList<>();
        turrets = new ArrayList<>();
        gunpowders = new ArrayList<>();
        for (Point2D enemy : enemyPositions) {
            enemies.add(new Enemy((double) enemy.getX(), (double) enemy.getY()));
        }
        for (Point2D gunpowder : gunpowderPositions) {
            gunpowders.add(new Gunpowder(gunpowder.getX(), gunpowder.getY()));
        }
        for (double[] turret : turretData) {
            turrets.add(new Turret(turret[Turret.TURRET_X], turret[Turret.TURRET_Y], turret[Turret.TURRET_SPEED], TurretDirection.getFacing(turret[Turret.TURRET_FACE])));
        }
    }

    @Override
    public void draw() {
        level.draw();
        finish.draw();

        for (Turret turret : turrets) {
            turret.draw();
        }

        player.draw();

        for (Enemy enemy : enemies) {
            enemy.draw();
        }
        ParticleGenerator.drawParticles();
        
        for (Gunpowder gunpowder : gunpowders) {
            gunpowder.draw();
        }
    }

    @Override
    public void processInputs(double delta) {
        while (Keyboard.next()) {
            if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
                NinjaCreeper.gameState = NinjaCreeper.menu;
            }
        }
        player.movements(delta);
        finish.movements(delta);
        for (Enemy enemy : enemies) {
            if (enemy.isEnemyOnLava() || enemy.pos_y > NinjaCreeper.HEIGHT) {
                enemies.remove(enemy);
                break;
            }
            enemy.movements(delta);
        }
        for (Turret turret : turrets) {
            turret.physics(delta);
        }
        for (Gunpowder gunpowder : gunpowders) {
            gunpowder.movements(delta);
        }
        ParticleGenerator.particlePhysics(delta);
    }

    public static Level getCurrentLevel() {
        return level;
    }
}

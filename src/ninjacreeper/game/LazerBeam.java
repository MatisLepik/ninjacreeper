package ninjacreeper.game;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.util.Set;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.Color;

/**
 *
 * @author Matis
 */
public class LazerBeam extends Entity {

    Color beamColor = new Color(255 / 255f, 75 / 255f, 0 / 255f, 0.75f);
    private boolean hasImpacted = false;
    private final TurretDirection direction;
    private double height;
    private double width;

    public LazerBeam(double pos_x, double pos_y, TurretDirection direction, EntityType entityType) {
        super(pos_x + direction.getBulletSpawnX(), pos_y + direction.getBulletSpawnY(), entityType);
        this.speed_x = direction.getSpeedX();
        this.speed_y = direction.getSpeedY();
        this.direction = direction;
        this.height = entityType.getHeight();
        this.width = entityType.getWidth();
    }

    @Override
    public void draw() {
        glPushMatrix();
        glDisable(GL_TEXTURE_2D);
        glTranslated(pos_x, pos_y, 0);
        DoubleBuffer vertexData = BufferUtils.createDoubleBuffer(8);
        vertexData.put(new double[]{0, 0, 0, height, width, height, width, 0});
        FloatBuffer colorData = BufferUtils.createFloatBuffer(16);
        colorData.put(new float[]{beamColor.r, beamColor.g, beamColor.b, beamColor.a, beamColor.r, beamColor.g, beamColor.b, beamColor.a, beamColor.r, beamColor.g, beamColor.b, beamColor.a, beamColor.r, beamColor.g, beamColor.b, beamColor.a});

        NinjaCreeper.draw2DQuadColored(vertexData, colorData);

        glPopMatrix();
        glDisableClientState(GL_COLOR_ARRAY);
        glDisableClientState(GL_VERTEX_ARRAY);
    }

    @Override
    public void physics() {
    }

    @Override
    public void movements(double delta) {
        if (pos_y <= NinjaCreeper.HEIGHT) {
            Set<Character> blocksBelow = getHorizontalBlocks(pos_y + height + EntityType.MINVALUE, pos_x, pos_x + width);
            Set<Character> blocksRight = getVerticalBlocks(pos_x + width + EntityType.MINVALUE, pos_y, pos_y + height);
            Set<Character> blocksLeft = getVerticalBlocks(pos_x - EntityType.MINVALUE, pos_y, pos_y + height);
            Set<Character> blocksAbove = getHorizontalBlocks(pos_y - EntityType.MINVALUE, pos_x, pos_x + width);

            if (blocksAbove.contains(Level.WALL) || blocksAbove.contains(Level.ICE) || blocksAbove.contains(Level.RUBBER)
                    || blocksBelow.contains(Level.WALL) || blocksBelow.contains(Level.ICE) || blocksBelow.contains(Level.RUBBER)) {
                if (direction == TurretDirection.DOWN) {
                    height -= speed_y * delta;
                } else if (direction == TurretDirection.UP) {
                    height += speed_y * delta;
                }
            }
            if (blocksLeft.contains(Level.WALL) || blocksLeft.contains(Level.ICE) || blocksLeft.contains(Level.RUBBER)
                    || blocksRight.contains(Level.WALL) || blocksRight.contains(Level.ICE) || blocksRight.contains(Level.RUBBER)) {
                if (direction == TurretDirection.RIGHT) {
                    width -= speed_x * delta;
                } else if (direction == TurretDirection.LEFT) {
                    width += speed_x * delta;
                }
            }

            if (height <= 0 || width <= 0) {
                hasImpacted = true;
            }

            if (pos_y <= GameScreen.player.pos_y + 36 && pos_y + height >= GameScreen.player.pos_y + 28
                    && pos_x <= GameScreen.player.pos_x + 24 && pos_x + width >= GameScreen.player.pos_x) { //collision with feet
                NinjaCreeper.endGame(NinjaCreeper.RESULT_FAIL_LAZER);
            }
            if (pos_y <= GameScreen.player.pos_y + 28 && pos_y + height >= GameScreen.player.pos_y
                    && pos_x <= GameScreen.player.pos_x + 20 && pos_x + width >= GameScreen.player.pos_x + 4) { //collision with body
                NinjaCreeper.endGame(NinjaCreeper.RESULT_FAIL_LAZER);
            }

            moveEntity(delta);
        }
    }

    public boolean hasImpacted() {
        return hasImpacted;
    }

    @Override
    public void moveEntity(double delta) {

        double distanceToMoveHorizontally = speed_x * delta;
        double distanceToMoveVertically = speed_y * delta;

        while (distanceToMoveHorizontally > 0) {
            Set<Character> blocks = getVerticalBlocks(pos_x + width, pos_y, pos_y + height);
            if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                pos_x += EntityType.MINVALUE;
            }
            distanceToMoveHorizontally -= EntityType.MINVALUE;
        }
        while (distanceToMoveHorizontally < 0) {
            Set<Character> blocks = getVerticalBlocks(pos_x - EntityType.MINVALUE, pos_y, pos_y + height);
            if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                pos_x -= EntityType.MINVALUE;
            }
            distanceToMoveHorizontally += EntityType.MINVALUE;
        }
        while (distanceToMoveVertically < 0) {
            Set<Character> blocks = getHorizontalBlocks(pos_y, pos_x, pos_x + width);
            if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                pos_y -= EntityType.MINVALUE;

            }
            distanceToMoveVertically += EntityType.MINVALUE;
        }
        while (distanceToMoveVertically > 0) {
            Set<Character> blocks = getHorizontalBlocks(pos_y + height + EntityType.MINVALUE, pos_x, pos_x + width);
            if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                pos_y += EntityType.MINVALUE;
            }
            distanceToMoveVertically -= EntityType.MINVALUE;
        }

    }

}

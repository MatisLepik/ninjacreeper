package ninjacreeper.game;

import ninjacreeper.menu.MusicPlayer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import ninjacreeper.menu.*;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.opengl.Texture;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.opengl.CursorLoader;
import org.newdawn.slick.opengl.PNGImageData;
import org.newdawn.slick.opengl.TextureLoader;

/**
 *
 * @author Matis
 */
public class NinjaCreeper {

    public static final int OPT_UP = 0, OPT_LEFT = 1, OPT_DOWN = 2, OPT_RIGHT = 3, OPT_LEAP = 4, OPT_MUSIC_VOLUME = 5, OPT_FX_VOLUME = 6, OPT_MUSIC_MUTED = 7, OPT_FX_MUTED = 8, OPT_LEVEL1 = 9, OPT_LEVEL2 = 10, OPT_LEVEL3 = 11, OPT_LEVEL4 = 12, OPT_LEVEL5 = 13, OPT_LEVEL6 = 14, OPT_LEVEL7 = 15, OPT_LEVEL8 = 16;
    public static final int RESULT_FINISH = 1, RESULT_FAIL_LAVA = 2, RESULT_FAIL_LAZER = 3, RESULT_FAIL_CAT = 4, RESULT_FAIL_OUTOFBOUNDS = 5;
    public static final int TARGET_FPS = 90, WIDTH = 800, HEIGHT = 600;
    public static final Color COLOR_TEXT = new Color(255 / 255f, 246 / 255f, 229 / 255f), COLOR_HIGHLIGHT = new Color(126 / 255f, 206 / 255f, 253 / 255f), COLOR_PURPLE = new Color(29 / 255f, 24 / 255f, 31 / 255f);
    public static final String TITLE = "NinjaCreeper";
    public static AngelCodeFont FONT_SMALL, FONT_MEDIUM;
    //   public static Texture TEX_LENA;
    public static Texture TEX_SPRITESHEET;
    public static double secondsPlayed = 0, currentFPS = 0, lastTime = getTime();
    private static long lastFrame = getTime();
    public static double[] options;
    public static boolean isGameRunning = false;
    public static GameState game, menu, end, gameState;

    public static void main(String[] args) {
        setIcon();
        try {
//            DisplayMode displayMode = null;
//            DisplayMode[] modes = Display.getAvailableDisplayModes();
//            for (DisplayMode mode : modes) {
//                if (mode.getWidth() == WIDTH && mode.getHeight() == HEIGHT && mode.isFullscreenCapable()) {
//                    displayMode = mode;
//                }
//            }

            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
            Display.setFullscreen(true);
            Display.setTitle(TITLE);
            Display.create();
            Keyboard.create();
            Mouse.create();
        } catch (LWJGLException ex) {
            ex.printStackTrace();
        }

        initializeOGL();
        loadResources();
        loadOptions();
        setCursor();
        FxPlayer.startPlayer();
        MusicPlayer.startPlayer();

        NinjaCreeper main = new NinjaCreeper();

        NinjaCreeper.TEX_SPRITESHEET.bind();
        Color.white.bind();

        menu = new MenuScreen();
        game = new GameScreen();
        end = new EndScreen();
        gameState = menu;

        gameLoop();

        Display.destroy();
        System.exit(0);
    }

    private static void gameLoop() {
        while (!Display.isCloseRequested()) {
            secondTimer();
            MusicPlayer.checkSongStatus();
            glClear(GL_COLOR_BUFFER_BIT);
            gameState.draw();
            gameState.processInputs(getDelta());
            Display.update();
            Display.sync(TARGET_FPS);
        }
    }

    public static void draw2DQuadTextured(DoubleBuffer vertexData, DoubleBuffer textureData) {
        textureData.flip();
        vertexData.flip();

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);

        glVertexPointer(2, 0, vertexData);
        glTexCoordPointer(2, 0, textureData);
        glDrawArrays(GL_QUADS, 0, 4);
    }

    public static void draw2DQuadColored(DoubleBuffer vertexData, FloatBuffer colorData) {
        vertexData.flip();
        colorData.flip();

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);

        glVertexPointer(2, 0, vertexData);
        glColorPointer(4, 0, colorData);
        glDrawArrays(GL_QUADS, 0, 4);
    }

    public static double getPointX(double pixelX) {
        if (pixelX == 0) {
            return 0;
        }
        return pixelX / TEX_SPRITESHEET.getTextureWidth();
    }

    public static double getPointY(double pixelY) {
        if (pixelY == 0) {
            return 0;
        }
        return pixelY / TEX_SPRITESHEET.getTextureHeight();
    }

    public static double getOGLColor(double input) {
        return input / 255;
    }

    public static BufferedImage getResourceImage(String path) {
        try {
            return ImageIO.read(NinjaCreeper.class.getResourceAsStream(path));
        } catch (IOException ex) {
            System.out.println("Error: " + ex);
            return null;
        }

    }

    public static int getMouseX() {
        return Mouse.getX();
    }

    public static int getMouseY() {
        return HEIGHT - Mouse.getY() - 1;
    }

    public static Texture getTexture(String fileName) {
        try {
            return TextureLoader.getTexture("PNG", NinjaCreeper.class.getResourceAsStream(fileName), GL_NEAREST);
        } catch (IOException | NullPointerException ex) {
            if (fileName.endsWith("foreground.png")) {
                System.out.println("Foreground not found");
            } else {
                ex.printStackTrace();
            }
            return null;
        }
    }

    public static void saveOptions() {
        File file = new File("options.dat");
        file.delete();
        try {
            file.createNewFile();
            FileOutputStream fileStream = new FileOutputStream(file);
            ObjectOutputStream output = new ObjectOutputStream(fileStream);
            output.writeObject(options);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private static void setIcon() {
        try {
            PNGImageData iconsmall = new PNGImageData();
            PNGImageData iconmed = new PNGImageData();
            PNGImageData iconlarge = new PNGImageData();
            iconsmall.loadImage(NinjaCreeper.class.getResourceAsStream("/ninjacreeper/resources/images/icon16.png"));
            iconmed.loadImage(NinjaCreeper.class.getResourceAsStream("/ninjacreeper/resources/images/icon32.png"));
            iconlarge.loadImage(NinjaCreeper.class.getResourceAsStream("/ninjacreeper/resources/images/icon128.png"));

            ByteBuffer buffer[] = (new ByteBuffer[]{
                iconsmall.getImageBufferData(),
                iconmed.getImageBufferData(),
                iconlarge.getImageBufferData()
            });
            Display.setIcon(buffer);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void setCursor() {
        try {
            Cursor cur = createCursor("/ninjacreeper/resources/images/cursorregular.png");
            Mouse.setNativeCursor(cur);
        } catch (LWJGLException ex) {
            ex.printStackTrace();
        }
    }

    private static Cursor createCursor(String path) {
        try {
            PNGImageData png = new PNGImageData();
            png.loadImage(NinjaCreeper.class.getResourceAsStream(path));
            CursorLoader c = CursorLoader.get();
            Cursor cur = c.getCursor(png, 1, 1);
            return cur;
        } catch (IOException | LWJGLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private static void initializeOGL() {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, WIDTH, HEIGHT, 0, 1, -1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
    }

    public static void startGame(int number, File directory) {
        if (loadResources(number, directory)) {
            isGameRunning = true;
            lastFrame = getTime();
            secondsPlayed = 0;
            NinjaCreeper.gameState = NinjaCreeper.game;
        } else {
            JOptionPane.showMessageDialog(null, "Could not load level.");
        }
    }

    private static boolean loadResources(int number, File directory) {
        if (number == 0) {
            return ResourceLoader.loadCustomFiles(directory);
        } else {
            return ResourceLoader.loadDefaultFiles(number);
        }
    }

    public static void endGame(int result) {
        double time = Math.round(secondsPlayed * 100.0) / 100.0;
        if (GameScreen.level.getLevelId() == 0) {
            EndScreen.setScreen(result, time, 0, false);
        } else {
            int levelPosition = 8 + GameScreen.level.getLevelId();
            boolean isRecord = options[levelPosition] > time || options[levelPosition] == 0;

            EndScreen.setScreen(result, time, GameScreen.level.getLevelId(), isRecord);

            if (result == RESULT_FINISH && isRecord) {
                options[levelPosition] = time;
                saveOptions();
            }
        }

        if (result == RESULT_FINISH) {
            FxPlayer.playSound(AudioFx.WIN);
        } else {
            FxPlayer.playSound(AudioFx.LOSE);
        }
        gameState = end;
        EndScreen.popAnimation();
    }

    private static void loadOptions() {
        File file = new File("options.dat");
        if (file.exists()) {
            try {
                FileInputStream fileStream = new FileInputStream(file);
                ObjectInputStream input = new ObjectInputStream(fileStream);
                options = (double[]) input.readObject();
            } catch (IOException | ClassNotFoundException ex) {
                System.out.println(ex);
                file.delete();
                createOptions();
            }
        } else {
            createOptions();
        }

    }

    private static void createOptions() {
        // Order: UP LEFT DOWN RIGHT LEAP, default W A S D SHIFT. Last 4 spots: Music vol, fx vol, music muted, fx muted, top scores
        options = new double[]{Keyboard.KEY_W, Keyboard.KEY_A, Keyboard.KEY_S, Keyboard.KEY_D, Keyboard.KEY_LSHIFT, 50, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        saveOptions();
    }

    private static void loadResources() {
        //  TEX_LENA = getTexture("/ninjacreeper/resources/images/lena.png");
        TEX_SPRITESHEET = getTexture("/ninjacreeper/resources/images/spritesheet.png");
        try {
            FONT_SMALL = new AngelCodeFont("small", NinjaCreeper.class.getResourceAsStream("/ninjacreeper/resources/fonts/hardpixelsmall.fnt"), NinjaCreeper.class.getResourceAsStream("/ninjacreeper/resources/fonts/hardpixelsmall.png"));
            FONT_MEDIUM = new AngelCodeFont("medium", NinjaCreeper.class.getResourceAsStream("/ninjacreeper/resources/fonts/hardpixelmedium.fnt"), NinjaCreeper.class.getResourceAsStream("/ninjacreeper/resources/fonts/hardpixelmedium.png"));

        } catch (SlickException ex) {
            ex.printStackTrace();
        }
    }

    public static long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }

    private static double getDelta() {
        long currentTime = getTime();
        double delta = (double) currentTime - (double) lastFrame;
        lastFrame = getTime();
        if (gameState == game) {
            secondsPlayed += delta / 1000;
        }
        return delta;
    }

    private static void secondTimer() {
        if (getTime() - lastTime > 1000) {
            currentFPS = 0;
            lastTime += 1000;
        }
        currentFPS++;
    }

    private NinjaCreeper() {

    }
}

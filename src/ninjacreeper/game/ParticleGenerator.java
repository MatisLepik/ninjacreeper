package ninjacreeper.game;

import java.util.ArrayList;
import org.newdawn.slick.Color;

/**
 *
 * @author Matis
 */
public class ParticleGenerator {

    private static final ArrayList<Particle> explosionList = new ArrayList<>();

    public static void createExplosion_creeperJump(double posX, double posY, EntityType ent, double speedX) {
        int particleCount = (int) (35 + Math.random() * 10);
        for (int i = particleCount; i > 0; i--) {
            double fallAccel = 0.0001;
            double pos_x = posX + Math.random() * ent.getWidth();
            double pos_y = posY + Math.random() * ent.getHeight();
            double speed_x = 0.001-speedX/2;
            double speed_y = -0.01 + Math.random() * 0.02;
            double lifeSpan = 150 + Math.random() * 300;
            Particle particle = new Particle(fallAccel, pos_x, pos_y, speed_x, speed_y, lifeSpan, new Color(85 / 255f, 190 / 255f, 70 / 255f), EntityType.EXPLOSIONPARTICLE, Level.WALL);
            explosionList.add(particle);
        }
    }

    public static void createSpeedParticle(double startX, double endY, double groundCoord, double xSpeed) {
        double fallAccel = 0.01;
        double pos_x = startX - 2 + Math.random() * 0.4;
        double pos_y = endY;
        double speed_x = xSpeed + Math.random() * xSpeed / 2;
        double speed_y = -0.15 + Math.random() * 0.15;
        double lifeSpan = 150 + Math.random() * 300;
        Particle particle = new Particle(fallAccel, pos_x, pos_y, speed_x, speed_y, lifeSpan, new Color(50 / 255f, 50 / 255f, 255 / 255f), EntityType.SPEEDPARTICLE, groundCoord);
        explosionList.add(particle);
    }
    
    public static void createSlideParticle(double posX, double posY, double wallCoord, double ySpeed, boolean againstLeftWall) {
        double fallAccel = 0.002;
        double pos_x = posX;
        double pos_y = posY;
        double speed_x;
        if (againstLeftWall) {
            speed_x = 0.01 + Math.random() * 0.02;
        } else {
            speed_x = -0.01 - Math.random() * -0.02;
        }
        double speed_y = -0.01;
        double lifeSpan = 100 + Math.random() * 200;
        Particle particle = new Particle(fallAccel, pos_x, pos_y, speed_x, speed_y, lifeSpan, new Color(50 / 255f, 50 / 255f, 255 / 255f), EntityType.SLIDEPARTICLE, wallCoord);
        explosionList.add(particle);
    }

    public static void drawParticles() {
        for (Particle particle : explosionList) {
            particle.draw();
        }
    }

    public static void particlePhysics(double delta) {
        ArrayList<Particle> particlesToRemove = new ArrayList<>(0);
        for (Particle particle : explosionList) {
            particle.movements(delta);

            if (particle.getAge() >= particle.getLifeSpan()) {
                particlesToRemove.add(particle);
            }
        }
        for (Particle particleToRemove : particlesToRemove) {
            explosionList.remove(particleToRemove);
        }
        particlesToRemove.clear();
    }
}

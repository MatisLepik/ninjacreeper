
package ninjacreeper.game;

/**
 *
 * @author Matis
 */
public class Finish extends Entity {
    
    private static double finishX, finishY;
    
    public Finish(double pos_x, double pos_y) {
        super(pos_x, pos_y, EntityType.FINISH);
    }
    
    public static void setPosition(double x, double y) {
        finishX = x;
        finishY = y;
    }

    @Override
    public void physics() {
    }
    
}


package ninjacreeper.game;

/**
 *
 * @author Matis
 */
public abstract class GameState {
    
    public abstract void draw();
    public abstract void processInputs(double delta);
    
}

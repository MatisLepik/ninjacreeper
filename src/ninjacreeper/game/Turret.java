package ninjacreeper.game;

import java.util.ArrayList;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Matis
 */
public class Turret {

    public static final int TURRET_X = 0;
    public static final int TURRET_Y = 1;
    public static final int TURRET_SPEED = 2;
    public static final int TURRET_FACE = 3;
    public static final double TURRETFACE_UP = 0;
    public static final double TURRETFACE_DOWN = 1;
    public static final double TURRETFACE_LEFT = 2;
    public static final double TURRETFACE_RIGHT = 3;

    private final TurretDirection turretDirection;
    private final double pos_x;
    private final double pos_y;
    private final double speed;

    private final double initialDelay = 250;
    private double timeLastFired = NinjaCreeper.getTime();

    private final ArrayList<LazerBeam> lazerBeams = new ArrayList<>(0);

    public Turret(double pos_x, double pos_y, double speed, TurretDirection turretDirection) {
        this.turretDirection = turretDirection;
        this.pos_x = pos_x;
        this.pos_y = pos_y;
        this.speed = speed;
        timeLastFired = NinjaCreeper.getTime() - speed * 1000 + initialDelay;
    }

    public void draw() {
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glTranslated(pos_x, pos_y, 0);
        glBegin(GL_QUADS);

        glTexCoord2d(turretDirection.getTopLeft().getX(), turretDirection.getTopLeft().getY());// Top left
        glVertex2d(0, 0); // Top left

        glTexCoord2d(turretDirection.getBottomLeft().getX(), turretDirection.getBottomLeft().getY()); // Bottom left
        glVertex2d(0, turretDirection.getHeight()); // Bottom left

        glTexCoord2d(turretDirection.getBottomRight().getX(), turretDirection.getBottomRight().getY());// Bottom right
        glVertex2d(turretDirection.getWidth(), turretDirection.getHeight()); // Bottom right

        glTexCoord2d(turretDirection.getTopRight().getX(), turretDirection.getTopRight().getY());// Top right
        glVertex2d(turretDirection.getWidth(), 0); // Top right

        glEnd();
        glPopMatrix();

        drawLazerBeams();
    }

    public void physics(double delta) {
        if ((NinjaCreeper.getTime() - timeLastFired) >= speed * 1000) {
            makeLazerBeam();
            timeLastFired = NinjaCreeper.getTime();
        }
        for (LazerBeam beam : lazerBeams) {
            if (beam.hasImpacted()) {
                //    FxPlayer.playSound(AudioFx.LAZERIMPACT);
                lazerBeams.remove(beam);
                break;
            }
            if (beam.pos_y < 0 || beam.pos_y + EntityType.LAZERBEAM_VERT.getHeight() > NinjaCreeper.HEIGHT || beam.pos_x + EntityType.LAZERBEAM_HOR.getWidth() < 0 || beam.pos_x > NinjaCreeper.WIDTH) {
                lazerBeams.remove(beam);
                break;
            }
            beam.movements(delta);
        }
    }

    private void makeLazerBeam() {
        if (turretDirection == TurretDirection.DOWN || turretDirection == TurretDirection.UP) {
            lazerBeams.add(new LazerBeam(pos_x, pos_y, turretDirection, EntityType.LAZERBEAM_VERT));
        } else {
            lazerBeams.add(new LazerBeam(pos_x, pos_y, turretDirection, EntityType.LAZERBEAM_HOR));
        }
        FxPlayer.playSound(AudioFx.LAZER);
    }

    private void drawLazerBeams() {
        for (LazerBeam beam : lazerBeams) {
            beam.draw();
        }
    }

}

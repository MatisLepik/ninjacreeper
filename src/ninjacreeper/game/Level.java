package ninjacreeper.game;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.newdawn.slick.opengl.Texture;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Matis
 */
public class Level {

    public static final char WALL = 'w', HAZARD = 'h', ICE = 'i', RUBBER = 'r', AIR = 'x', STEP = 's';
    public static final int VERTICALBLOCKS = 120, HORIZONTAlBLOCKS = 160;
    public static final double TEX_WIDTH = 5, TEX_HEIGHT = 5, Y_HAZARD = 249, Y_RUBBER = 254, Y_ICE = 259, Y_GROUND = 264, Y_EARTH = 269;

    private final Texture backgroundImage, foregroundImage;
    private final char[][] blockData;
    public final int levelId;
    public int tex_startX = 0;

    public Level(Texture backgroundImage, Texture foregroundImage, char[][] blockData, int levelId) {
        if (levelId == 1 || levelId == 2 || levelId == 0) {
            tex_startX = 705;
        } else if (levelId == 3 || levelId == 4) {
            tex_startX = 705;
        } else if (levelId == 5 || levelId == 6) {
            tex_startX = 710;
        } else if (levelId == 7 || levelId == 8) {
            tex_startX = 715;
        }

        this.levelId = levelId;
        this.blockData = blockData;
        this.backgroundImage = backgroundImage;
        this.foregroundImage = foregroundImage;
    }

    public void draw() {
        drawForegroundBackground();
        drawBlocks();
    }

    public char getBlockAt(double coordX, double coordY) {
        int blockX = (int) (coordX / 5);
        int blockY = (int) (coordY / 5);
        if (blockX >= 160 || blockY >= 120 || blockX < 0 || blockY < 0) {
            return 'x';
        }
        return blockData[blockX][blockY];
    }

    public int getLevelId() {
        return levelId;
    }

    private void drawForegroundBackground() {
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);

        DoubleBuffer vertexData = BufferUtils.createDoubleBuffer(8);
        vertexData.put(new double[]{0, 0, 0, NinjaCreeper.HEIGHT, NinjaCreeper.WIDTH, NinjaCreeper.HEIGHT, NinjaCreeper.WIDTH, 0});
        DoubleBuffer textureData = BufferUtils.createDoubleBuffer(8);
        textureData.put(new double[]{0, 0, 0, 600.0 / backgroundImage.getTextureHeight(), 800.0 / backgroundImage.getTextureWidth(), 600.0 / backgroundImage.getTextureHeight(), 800.0 / backgroundImage.getTextureWidth(), 0});

        backgroundImage.bind();
        NinjaCreeper.draw2DQuadTextured(vertexData, textureData);
        if (foregroundImage != null) {
            foregroundImage.bind();
            NinjaCreeper.draw2DQuadTextured(vertexData, textureData);
        }

        glPopMatrix();
        glDisable(GL_TEXTURE_2D);
        NinjaCreeper.TEX_SPRITESHEET.bind();
    }

    private void drawBlocks() {
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
        for (int x = 0; x < HORIZONTAlBLOCKS; x++) {
            for (int y = 0; y < VERTICALBLOCKS; y++) {
                switch (blockData[x][y]) {
                    case WALL:
                        if (y != 0 && blockData[x][y - 1] == AIR || y != 0 && blockData[x][y - 1] == '\u0000') {
                            drawRectangle(x, y, Y_GROUND);
                        } else {
                            drawRectangle(x, y, Y_EARTH);
                        }

                        break;
                    case HAZARD:
                        drawRectangle(x, y, Y_HAZARD);
                        break;
                    case ICE:
                        drawRectangle(x, y, Y_ICE);
                        break;
                    case RUBBER:
                        drawRectangle(x, y, Y_RUBBER);
                        break;
                }
            }
        }
        glEnd();
        glPopMatrix();
    }

    private void drawRectangle(double x, double y, double textureY) {
        glTexCoord2d(NinjaCreeper.getPointX(tex_startX), NinjaCreeper.getPointY(textureY));// Top left
        glVertex2d(x * 5, y * 5); // Top left

        glTexCoord2d(NinjaCreeper.getPointX(tex_startX), NinjaCreeper.getPointY(textureY + TEX_HEIGHT)); // Bottom left
        glVertex2d(x * 5, y * 5 + TEX_HEIGHT); // Bottom left

        glTexCoord2d(NinjaCreeper.getPointX(tex_startX + TEX_WIDTH), NinjaCreeper.getPointY(textureY + TEX_HEIGHT));// Bottom right
        glVertex2d(x * 5 + TEX_WIDTH, y * 5 + TEX_HEIGHT); // Bottom right

        glTexCoord2d(NinjaCreeper.getPointX(tex_startX + TEX_WIDTH), NinjaCreeper.getPointY(textureY));// Top right
        glVertex2d(x * 5 + TEX_WIDTH, y * 5); // Top right
    }

}

package ninjacreeper.game;

import ninjacreeper.menu.MusicPlayer;
import java.io.BufferedInputStream;
import java.io.IOException;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;

/**
 *
 * @author Matis
 */
public class FxPlayer {

    private static boolean isFxMuted = false;
    private static int fxVolume = 50;

    public static void startPlayer() {
        refreshFxMuted();
        refreshFxVolume();
    }

    public static void refreshFxVolume() {
        fxVolume = (int)NinjaCreeper.options[NinjaCreeper.OPT_FX_VOLUME];
    }

    public static int getFxVolume() {
        refreshFxVolume();
        return fxVolume;
    }

    public static void refreshFxMuted() {
        isFxMuted = NinjaCreeper.options[NinjaCreeper.OPT_FX_MUTED] == 1;
    }

    public static boolean getFxMuted() {
        refreshFxMuted();
        return isFxMuted;
    }

    public static void playSound(AudioFx fx) {
        float pitch = 1.0f;
        float vol;
        if (fx == AudioFx.STEP) {
            pitch = 0.5f + (float) Math.random() * 0.5f;
        }
        if (fx == AudioFx.WIN || fx == AudioFx.LOSE) {
            vol = MusicPlayer.getMusicVolume();
        } else if (fx == AudioFx.LAZER) {
            vol = fxVolume / 4;
        } else {
            vol = fxVolume;
        }
        if (!isFxMuted) {
            try {
                Audio wavEffect = AudioLoader.getAudio("WAV", new BufferedInputStream(NinjaCreeper.class.getResourceAsStream(fx.getPath())));
                wavEffect.playAsSoundEffect(pitch, vol / 100f, false);
            } catch (IOException | NullPointerException ex) {
                ex.printStackTrace();
            }
        }

    }

}

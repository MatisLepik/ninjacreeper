package ninjacreeper.game;

import java.nio.DoubleBuffer;
import java.util.HashSet;
import java.util.Set;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Matis
 */
public abstract class Entity {

    public boolean hasJumped = false, hasDoubleJumped = false, hasWallJumped = false;
    double speed_x = 0, speed_y = 0;
    double pos_x, pos_y;
    private long lastWalkAnimation = NinjaCreeper.getTime();
    EntityType entityType;
    boolean facingLeft = true;
    boolean isRunningAgainstLeftWall = false;
    boolean isRunningAgainstRightWall = false;
    public static final double STEPSIZE = 5;
    private double runningAnimationCount = 0;

    public abstract void physics();

    public Entity(double pos_x, double pos_y, EntityType entityType) {
        this.pos_x = pos_x;
        this.pos_y = pos_y;
        this.entityType = entityType;
    }

    public void movements(double delta) {
        physics();
        moveEntity(delta);
    }

    public void draw() {
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glTranslated(pos_x, pos_y, 0);

        DoubleBuffer vertexData = BufferUtils.createDoubleBuffer(8);
        vertexData.put(new double[]{0, 0, 0, entityType.getHeight(), entityType.getWidth(), entityType.getHeight(), entityType.getWidth(), 0});

        DoubleBuffer textureData = BufferUtils.createDoubleBuffer(8);
        if (facingLeft) {
            textureData.put(new double[]{NinjaCreeper.getPointX(entityType.getTextureX(isRunningAgainstLeftWall || isRunningAgainstRightWall, speed_x, runningAnimationCount)), NinjaCreeper.getPointY(entityType.getTextureY()),
                NinjaCreeper.getPointX(entityType.getTextureX(isRunningAgainstLeftWall || isRunningAgainstRightWall, speed_x, runningAnimationCount)), NinjaCreeper.getPointY(entityType.getTextureY() + entityType.getHeight()),
                NinjaCreeper.getPointX(entityType.getTextureX(isRunningAgainstLeftWall || isRunningAgainstRightWall, speed_x, runningAnimationCount) + entityType.getWidth()), NinjaCreeper.getPointY(entityType.getTextureY() + entityType.getHeight()),
                NinjaCreeper.getPointX(entityType.getTextureX(isRunningAgainstLeftWall || isRunningAgainstRightWall, speed_x, runningAnimationCount) + entityType.getWidth()), NinjaCreeper.getPointY(entityType.getTextureY())});
        } else {
            textureData.put(new double[]{NinjaCreeper.getPointX(entityType.getTextureX(isRunningAgainstLeftWall || isRunningAgainstRightWall, speed_x, runningAnimationCount) + entityType.getWidth()), NinjaCreeper.getPointY(entityType.getTextureY()),
                NinjaCreeper.getPointX(entityType.getTextureX(isRunningAgainstLeftWall || isRunningAgainstRightWall, speed_x, runningAnimationCount) + entityType.getWidth()), NinjaCreeper.getPointY(entityType.getTextureY() + entityType.getHeight()),
                NinjaCreeper.getPointX(entityType.getTextureX(isRunningAgainstLeftWall || isRunningAgainstRightWall, speed_x, runningAnimationCount)), NinjaCreeper.getPointY(entityType.getTextureY() + entityType.getHeight()),
                NinjaCreeper.getPointX(entityType.getTextureX(isRunningAgainstLeftWall || isRunningAgainstRightWall, speed_x, runningAnimationCount)), NinjaCreeper.getPointY(entityType.getTextureY())});
        }

        NinjaCreeper.draw2DQuadTextured(vertexData, textureData);

        glPopMatrix();
    }

    public void gravity(boolean inAir, Set<Character> blocksBelow) {
        if (inAir) {
            if ((isRunningAgainstLeftWall || isRunningAgainstRightWall) && speed_y > 0) {
                speed_y = Math.min(speed_y + (entityType.getFallAccel() * 0.1), 0.1);
            } else {
                speed_y = Math.min(speed_y + (entityType.getFallAccel()), entityType.getMaxFallSpeed());
            }
        } else {
            if (blocksBelow.contains(Level.RUBBER)) {
                if (speed_y > 0) {
                    speed_y = -speed_y - entityType.getRubberJumpBoost();
                    hasJumped = false;
                    hasDoubleJumped = false;
                    hasWallJumped = false;
                }
            } else if (blocksBelow.contains(Level.WALL) || blocksBelow.contains(Level.ICE)) {
                if (speed_y > 0) {
                    speed_y = 0;
                    hasJumped = false;
                    hasDoubleJumped = false;
                    hasWallJumped = false;
                }
            }
        }
    }

    public void movementAnimation(boolean inAir) {
        if (speed_x != 0 && lastWalkAnimation < NinjaCreeper.getTime() - entityType.getMoveAnimationSpeed(speed_x)) {
            if (!inAir) {
                runningAnimation();
                lastWalkAnimation = NinjaCreeper.getTime();
            } else {
                resetWalkTexture();
            }
        }
    }

    public void runningAnimation() {
        if (runningAnimationCount == 3) {
            runningAnimationCount = 0;
        } else {

            runningAnimationCount++;
        }
    }

    public void resetWalkTexture() {
        runningAnimationCount = 0;
    }

    public void moveEntity(double delta) {

        double distanceToMoveHorizontally = speed_x * delta;
        double distanceToMoveVertically = speed_y * delta;

        if (entityType == EntityType.PLAYER) {
            while (distanceToMoveHorizontally > 0) { //Moving right
                Set<Character> blocks = getRightBlocks_player(pos_x + entityType.getWidth(), pos_y, pos_y + entityType.getHeight());
                if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER) && !blocks.contains(Level.STEP)) {
                    pos_x += EntityType.MINVALUE;
                }
                distanceToMoveHorizontally -= EntityType.MINVALUE;
            }
            while (distanceToMoveHorizontally < 0) { //Moving left
                Set<Character> blocks = getLeftBlocks_player(pos_x - EntityType.MINVALUE, pos_y, pos_y + entityType.getHeight());
                if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER) && !blocks.contains(Level.STEP)) {
                    pos_x -= EntityType.MINVALUE;
                }
                distanceToMoveHorizontally += EntityType.MINVALUE;
            }
            while (distanceToMoveVertically < 0) { //Moving up
                Set<Character> blocks = getUpperBlocks_player(pos_y, pos_x, pos_x + entityType.getWidth());
                if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                    pos_y -= EntityType.MINVALUE;

                }
                distanceToMoveVertically += EntityType.MINVALUE;
            }
            while (distanceToMoveVertically > 0) { //Moving down
                Set<Character> blocks = getHorizontalBlocks(pos_y + entityType.getHeight() + EntityType.MINVALUE, pos_x, pos_x + entityType.getWidth());
                if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                    pos_y += EntityType.MINVALUE;
                }
                distanceToMoveVertically -= EntityType.MINVALUE;
            }
        } else {
            while (distanceToMoveHorizontally > 0) {
                Set<Character> blocks = getVerticalBlocks(pos_x + entityType.getWidth(), pos_y, pos_y + entityType.getHeight());
                if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                    pos_x += EntityType.MINVALUE;
                }
                distanceToMoveHorizontally -= EntityType.MINVALUE;
            }
            while (distanceToMoveHorizontally < 0) {
                Set<Character> blocks = getVerticalBlocks(pos_x - EntityType.MINVALUE, pos_y, pos_y + entityType.getHeight());
                if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                    pos_x -= EntityType.MINVALUE;
                }
                distanceToMoveHorizontally += EntityType.MINVALUE;
            }
            while (distanceToMoveVertically < 0) {
                Set<Character> blocks = getHorizontalBlocks(pos_y, pos_x, pos_x + entityType.getWidth());
                if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                    pos_y -= EntityType.MINVALUE;

                }
                distanceToMoveVertically += EntityType.MINVALUE;
            }
            while (distanceToMoveVertically > 0) {
                Set<Character> blocks = getHorizontalBlocks(pos_y + entityType.getHeight() + EntityType.MINVALUE, pos_x, pos_x + entityType.getWidth());
                if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                    pos_y += EntityType.MINVALUE;
                }
                distanceToMoveVertically -= EntityType.MINVALUE;
            }
        }
    }

    public Set<Character> getVerticalBlocks(double x, double yStart, double yEnd) {
        Set<Character> blocks = new HashSet<>();
        double currentY = yStart;
        while (currentY <= yEnd) {
            blocks.add(GameScreen.getCurrentLevel().getBlockAt(x, currentY));
            currentY += EntityType.MINVALUE;
        }
        return blocks;
    }

    public Set<Character> getHorizontalBlocks(double y, double xStart, double xEnd) {
        Set<Character> blocks = new HashSet<>();
        double currentX = xStart;
        while (currentX <= xEnd) {
            blocks.add(GameScreen.getCurrentLevel().getBlockAt(currentX, y));
            currentX += EntityType.MINVALUE;
        }
        return blocks;
    }

    public Set<Character> getUpperBlocks_player(double y, double xStart, double xEnd) {
        Set<Character> blocks = new HashSet<>();
        double currentY = y; //check headlevel
        double currentX = xStart + 4;
        double lastX = xEnd - 4;
        while (currentX <= lastX) {
            blocks.add(GameScreen.getCurrentLevel().getBlockAt(currentX, currentY));
            currentX += EntityType.MINVALUE;
        }
        currentY = y + 28; //check feetlevel
        currentX = xStart;
        lastX = xEnd;
        while (currentX <= lastX) {
            blocks.add(GameScreen.getCurrentLevel().getBlockAt(currentX, currentY));
            currentX += EntityType.MINVALUE;
        }
        return blocks;
    }

    public Set<Character> getLeftBlocks_player(double x, double yStart, double yEnd) {
        Set<Character> blocks = new HashSet<>();
        double currentX = x + 4; //check head level
        double currentY = yStart;
        double lastY = yStart + 28;
        while (currentY <= lastY) {
            blocks.add(GameScreen.getCurrentLevel().getBlockAt(currentX, currentY));
            currentY += EntityType.MINVALUE;
        }
        currentX = x; //check feet level
        currentY = yStart + 28;
        lastY = yEnd;
        while (currentY <= lastY) {
            if (currentY >= lastY - Player.STEPSIZE) {
                if (GameScreen.getCurrentLevel().getBlockAt(currentX, currentY) != Level.AIR) {
                    blocks.add(Level.STEP);
                } else {
                    blocks.add(Level.AIR);
                }
            } else {
                blocks.add(GameScreen.getCurrentLevel().getBlockAt(currentX, currentY));
            }
            currentY += EntityType.MINVALUE;
        }
        return blocks;
    }

    public Set<Character> getRightBlocks_player(double x, double yStart, double yEnd) {
        Set<Character> blocks = new HashSet<>();
        double currentX = x - 4; //check head level
        double currentY = yStart;
        double lastY = yStart + 28;
        while (currentY <= lastY) {
            blocks.add(GameScreen.getCurrentLevel().getBlockAt(currentX, currentY));
            currentY += EntityType.MINVALUE;
        }
        currentX = x; //check feet level
        currentY = yStart + 28;
        lastY = yEnd;
        while (currentY <= lastY) {
            if (currentY >= lastY - Player.STEPSIZE) {
                if (GameScreen.getCurrentLevel().getBlockAt(currentX, currentY) != Level.AIR) {
                    blocks.add(Level.STEP);
                } else {
                    blocks.add(Level.AIR);
                }
            } else {
                blocks.add(GameScreen.getCurrentLevel().getBlockAt(currentX, currentY));
            }
            currentY += EntityType.MINVALUE;
        }
        return blocks;
    }

    public Set<Character> getLeftBlocks_enemy(double x, double yStart, double yEnd) {
        Set<Character> blocks = new HashSet<>();
        double currentY = yStart;
        while (currentY <= yEnd) {
            blocks.add(GameScreen.getCurrentLevel().getBlockAt(x, currentY));
            currentY += EntityType.MINVALUE;
        }
        return blocks;
    }
}

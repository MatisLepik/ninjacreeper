package ninjacreeper.game;

import java.nio.DoubleBuffer;
import java.util.Set;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Matis
 */
public class Player extends Entity {

    private boolean jumpDown = false, wasInAir = false, touchingFinish = false;
    private long lastWalkSound = NinjaCreeper.getTime();
    private long lastSpeedParticle = NinjaCreeper.getTime();
    private long lastSlideParticle = NinjaCreeper.getTime();

    public Player(double pos_x, double pos_y) {
        super(pos_x, pos_y, EntityType.PLAYER);
        facingLeft = false;
    }

    @Override
    public void physics() {
        calculateSpeed();
    }

    private void calculateSpeed() {
        Set<Character> blocksBelow = getHorizontalBlocks(pos_y + entityType.getHeight() + EntityType.MINVALUE, pos_x, pos_x + entityType.getWidth());
        Set<Character> blocksAbove = getUpperBlocks_player(pos_y - EntityType.MINVALUE, pos_x, pos_x + entityType.getWidth());
        Set<Character> blocksRight = getRightBlocks_player(pos_x + entityType.getWidth() + EntityType.MINVALUE, pos_y, pos_y + entityType.getHeight());
        Set<Character> blocksLeft = getLeftBlocks_player(pos_x - EntityType.MINVALUE, pos_y, pos_y + entityType.getHeight());
        boolean inAir = !blocksBelow.contains(Level.RUBBER) && !blocksBelow.contains(Level.ICE) && !blocksBelow.contains(Level.WALL);
        boolean againstRightWall = blocksRight.contains(Level.WALL) || blocksRight.contains(Level.ICE) || blocksRight.contains(Level.RUBBER);
        boolean againstLeftWall = blocksLeft.contains(Level.WALL) || blocksLeft.contains(Level.ICE) || blocksLeft.contains(Level.RUBBER);
        boolean pressedJump = false;
        double maxMoveSpeed;
        double friction;

        checkCollision(blocksBelow, blocksAbove, blocksLeft, blocksRight);
        gravity(inAir, blocksBelow);

        if (Keyboard.getEventKey() == NinjaCreeper.options[NinjaCreeper.OPT_UP]) {
            if (!jumpDown && Keyboard.getEventKeyState()) {
                pressedJump = true;
            } else if (Keyboard.getEventKeyState() == false) {
                hasWallJumped = false;
            }
            jumpDown = Keyboard.getEventKeyState();
        }

        if (blocksBelow.contains(Level.ICE)) {
            maxMoveSpeed = entityType.getIceSpeed();
            friction = entityType.getIceFriction();
        } else {
            maxMoveSpeed = entityType.getMoveSpeed();
            friction = entityType.getMoveAccel();
        }
        if (Keyboard.isKeyDown((int) NinjaCreeper.options[NinjaCreeper.OPT_LEAP])) {
            maxMoveSpeed += entityType.getSprintSpeedBoost();
        }

//      Check if we can walljump
        isRunningAgainstLeftWall = false;
        isRunningAgainstRightWall = false;
        boolean justWallJumped = false;
        if (againstLeftWall && inAir) {
            isRunningAgainstLeftWall = true;
            facingLeft = false;
            hasDoubleJumped = false;
            if (pressedJump) {
                justWallJumped = true;
                hasWallJumped = true;
                speed_x = entityType.getWallJumpSpeed_horizontal();
                speed_y = -entityType.getWallJumpSpeed_vertical();
            }

        }
        if (againstRightWall && inAir) {
            isRunningAgainstRightWall = true;
            facingLeft = true;
            hasDoubleJumped = false;
            if (pressedJump) {
                justWallJumped = true;
                hasWallJumped = true;
                speed_x = -entityType.getWallJumpSpeed_horizontal();
                speed_y = -entityType.getWallJumpSpeed_vertical();
            }
        }

        //JUMPING
        if (pressedJump && !justWallJumped) {
            if (!hasJumped) {
                speed_y = -entityType.getJumpSpeed();
                hasJumped = true;
                if (inAir) {
                    ParticleGenerator.createExplosion_creeperJump(pos_x, pos_y, entityType, speed_x);
                }
            } else if (!hasDoubleJumped) {
                speed_y = -entityType.getJumpSpeed();
                FxPlayer.playSound(AudioFx.JUMPBOOST);
                hasDoubleJumped = true;
                ParticleGenerator.createExplosion_creeperJump(pos_x, pos_y, entityType, speed_x);
            }
        }

        //LEAPING
        if (Keyboard.isKeyDown((int) NinjaCreeper.options[NinjaCreeper.OPT_LEAP])) {
            //IMPLEMENT
        }

        //MOVING SIDEWAYS
        if (Keyboard.isKeyDown((int) NinjaCreeper.options[NinjaCreeper.OPT_RIGHT])) {

            if (!blocksRight.contains(Level.WALL) && !blocksRight.contains(Level.ICE) && !blocksRight.contains(Level.RUBBER)) { //No walls to the right
                if (blocksRight.contains(Level.STEP)) { //But we can make a step
                    if (canStep()) {
                        pos_y -= STEPSIZE;
                    } else {
                        speed_x = 0;
                        return;
                    }
                }
                if (speed_x < 0 && !hasWallJumped) {
                    //     speed_x = 0;
                }
                speed_x = Math.min(speed_x + entityType.getMoveAccel(), maxMoveSpeed);
                facingLeft = false;

            } else if (!hasWallJumped) { //We are against a wall
                speed_x = 0;
            }

        }
        if (Keyboard.isKeyDown((int) NinjaCreeper.options[NinjaCreeper.OPT_LEFT])) {

            if (!blocksLeft.contains(Level.WALL) && !blocksLeft.contains(Level.ICE) && !blocksLeft.contains(Level.RUBBER)) {
                if (blocksLeft.contains(Level.STEP)) {
                    if (canStep()) {
                        pos_y -= STEPSIZE;
                    } else {
                        speed_x = 0;
                        return;
                    }
                }
                if (speed_x > 0 && !hasWallJumped) {
                    //         speed_x = 0;
                }
                speed_x = Math.max(speed_x - entityType.getMoveAccel(), -maxMoveSpeed);
                facingLeft = true;
            } else if (!hasWallJumped) {
                speed_x = 0;
            }
        }
        //FRICTION
        if (!Keyboard.isKeyDown((int) NinjaCreeper.options[NinjaCreeper.OPT_RIGHT]) && !Keyboard.isKeyDown((int) NinjaCreeper.options[NinjaCreeper.OPT_LEFT])) {
            if (speed_x > 0) {
                speed_x = Math.max(speed_x - friction, 0);
            } else if (speed_x < 0) {
                speed_x = Math.min(speed_x + friction, 0);
            }
        }

        //Air friction down to max walk speed (to counteract leap extra speed)
        if (speed_x > maxMoveSpeed && inAir) {
            speed_x -= entityType.getMoveAccel() / 4;
        } else if (speed_x < -maxMoveSpeed && inAir) {
            speed_x += entityType.getMoveAccel() / 4;
        }

        //Speed particles
        if (speed_x > entityType.getMoveSpeed() && !inAir && lastSpeedParticle < NinjaCreeper.getTime() - 25) {
            ParticleGenerator.createSpeedParticle(pos_x, pos_y + entityType.getHeight(), getGroundTex(), -0.001);
            lastSpeedParticle = NinjaCreeper.getTime();
        } else if (speed_x < -entityType.getMoveSpeed() && !inAir && lastSpeedParticle < NinjaCreeper.getTime() - 25) {
            ParticleGenerator.createSpeedParticle(pos_x + entityType.getWidth(), pos_y + entityType.getHeight(), getGroundTex(), 0.001);
            lastSpeedParticle = NinjaCreeper.getTime();
        }

        //Wall slide particles
        if ((isRunningAgainstLeftWall || isRunningAgainstRightWall) && speed_y != 0 && lastSlideParticle < NinjaCreeper.getTime() - 50) {
            if (againstRightWall) {
                ParticleGenerator.createSlideParticle(pos_x + entityType.getWidth(), pos_y + entityType.getHeight(), getWallTex(false), 0.001, false);
            } else {
                ParticleGenerator.createSlideParticle(pos_x, pos_y + entityType.getHeight(), getWallTex(true), 0.001, true);
            }
            lastSlideParticle = NinjaCreeper.getTime();
        }

        //Movement sound
        if (speed_x != 0 && !inAir && lastWalkSound < NinjaCreeper.getTime() - 200) {
            FxPlayer.playSound(AudioFx.STEP);
            lastWalkSound = NinjaCreeper.getTime();
        }
        if (!inAir && wasInAir) {
            FxPlayer.playSound(AudioFx.STEP);
        }
        wasInAir = inAir;

        //Movement animation
        movementAnimation(inAir);
    }

    private void checkCollision(Set<Character> blocksBelow, Set<Character> blocksAbove, Set<Character> blocksLeft, Set<Character> blocksRight) {
        //COLLISION WITH HAZARD
        if (blocksBelow.contains(Level.HAZARD) || blocksAbove.contains(Level.HAZARD) || blocksLeft.contains(Level.HAZARD) || blocksRight.contains(Level.HAZARD)) {
            NinjaCreeper.endGame(NinjaCreeper.RESULT_FAIL_LAVA);
        }

        //OUT OF BOUNDS
        if (pos_y > NinjaCreeper.HEIGHT + entityType.getHeight()) {
            NinjaCreeper.endGame(NinjaCreeper.RESULT_FAIL_OUTOFBOUNDS);
            System.out.println("we are here now");
        }

        //COLLISION ABOVE
        if (blocksAbove.contains(Level.RUBBER)) {
            if (speed_y < 0) {
                speed_y = -speed_y - entityType.getRubberJumpBoost();
            }
        } else if (blocksAbove.contains(Level.WALL) || blocksAbove.contains(Level.ICE)) {
            if (speed_y < 0) {
                speed_y = 0;
            }
        }

        //COLLISION WITH FINISH
        if (pos_y + entityType.getHeight() >= GameScreen.finish.pos_y
                && pos_y <= GameScreen.finish.pos_y + GameScreen.finish.entityType.getHeight()
                && pos_x + entityType.getWidth() >= GameScreen.finish.pos_x
                && pos_x <= GameScreen.finish.pos_x + GameScreen.finish.entityType.getWidth()) {
            if (GameScreen.gunpowders.isEmpty()) {
                NinjaCreeper.endGame(NinjaCreeper.RESULT_FINISH);
            } else {
                touchingFinish = true;
            }
        } else {
            touchingFinish = false;
        }

        //COLLISION WITH GUNPOWDER
        for (Gunpowder gunpowder : GameScreen.gunpowders) {
            if (pos_y + entityType.getHeight() >= gunpowder.pos_y
                    && pos_y <= gunpowder.pos_y + gunpowder.entityType.getHeight()
                    && pos_x + entityType.getWidth() >= gunpowder.pos_x
                    && pos_x <= gunpowder.pos_x + gunpowder.entityType.getWidth()) {
                FxPlayer.playSound(AudioFx.PICKUP);
                GameScreen.gunpowders.remove(gunpowder);
                return;
            }
        }

        //COLLISION WITH ENEMY
        for (Enemy enemy : GameScreen.enemies) {
            if (pos_y + entityType.getHeight() >= enemy.pos_y
                    && pos_y <= enemy.pos_y + enemy.entityType.getHeight()
                    && pos_x + entityType.getWidth() >= enemy.pos_x
                    && pos_x <= enemy.pos_x + enemy.entityType.getWidth()) {
                NinjaCreeper.endGame(NinjaCreeper.RESULT_FAIL_CAT);
                //   return;
            }
        }
    }

    private double getGroundTex() {
        char particleBlock = GameScreen.getCurrentLevel().getBlockAt(pos_x + (entityType.getWidth() / 2), pos_y + entityType.getHeight() + 1);
        if (particleBlock == Level.RUBBER) {
            return Level.Y_RUBBER;
        } else if (particleBlock == Level.ICE) {
            return Level.Y_ICE;
        } else {
            return Level.Y_GROUND;
        }
    }

    private double getWallTex(boolean againstLeftWall) {
        char particleBlock;
        if (againstLeftWall) {
            particleBlock = GameScreen.getCurrentLevel().getBlockAt(pos_x - 1, pos_y + 28);
        } else {
            particleBlock = GameScreen.getCurrentLevel().getBlockAt(pos_x + entityType.getWidth() + 1, pos_y + 28);
        }
        if (particleBlock == Level.RUBBER) {
            return Level.Y_RUBBER;
        } else if (particleBlock == Level.ICE) {
            return Level.Y_ICE;
        } else {
            return Level.Y_EARTH;
        }

    }

    private boolean canStep() {
        double yPos = pos_y - STEPSIZE;
        while (yPos < pos_y) {
            Set<Character> blocksAbove = getUpperBlocks_player(yPos, pos_x, pos_x + entityType.getWidth());
            if (blocksAbove.contains(Level.WALL) || blocksAbove.contains(Level.ICE) || blocksAbove.contains(Level.RUBBER)) {
                return false;
            }
            yPos += EntityType.MINVALUE;
        }
        return true;

    }

    @Override
    public void draw() {
        super.draw();
        if (touchingFinish) {
            glPushMatrix();
            glEnable(GL_TEXTURE_2D);
            glEnable(GL_BLEND);
            glTranslated(pos_x + entityType.getWidth() / 2 - 16, pos_y - 35, 0);

            DoubleBuffer vertexData = BufferUtils.createDoubleBuffer(8);
            vertexData.put(new double[]{0, 0, 0, 28, 32, 28, 32, 0});

            DoubleBuffer textureData = BufferUtils.createDoubleBuffer(8);
            textureData.put(new double[]{NinjaCreeper.getPointX(700), NinjaCreeper.getPointY(310), NinjaCreeper.getPointX(700), NinjaCreeper.getPointY(338), NinjaCreeper.getPointX(732), NinjaCreeper.getPointY(338), NinjaCreeper.getPointX(732), NinjaCreeper.getPointY(310)});

            NinjaCreeper.draw2DQuadTextured(vertexData, textureData);

            glPopMatrix();
        } else if (GameScreen.level.levelId == 1) {
            if (pos_y > 225 && pos_x > 695) {
                glPushMatrix();
                glEnable(GL_TEXTURE_2D);
                glTranslated(pos_x-40, pos_y - 30, 0);

                DoubleBuffer vertexData = BufferUtils.createDoubleBuffer(8);
                vertexData.put(new double[]{0, 0, 0, 19, 97, 19, 97, 0});
                DoubleBuffer textureData = BufferUtils.createDoubleBuffer(8);
                textureData.put(new double[]{NinjaCreeper.getPointX(841), NinjaCreeper.getPointY(20), NinjaCreeper.getPointX(841), NinjaCreeper.getPointY(39), NinjaCreeper.getPointX(938), NinjaCreeper.getPointY(39), NinjaCreeper.getPointX(938), NinjaCreeper.getPointY(20)});
                NinjaCreeper.draw2DQuadTextured(vertexData, textureData);

                glDisable(GL_TEXTURE_2D);
                glPopMatrix();
            } else if (pos_y > 440 && pos_x > 535) {
                glPushMatrix();
                glEnable(GL_TEXTURE_2D);
                glTranslated(pos_x-40, pos_y - 30, 0);

                DoubleBuffer vertexData = BufferUtils.createDoubleBuffer(8);
                vertexData.put(new double[]{0, 0, 0, 19, 113, 19, 113, 0});
                DoubleBuffer textureData = BufferUtils.createDoubleBuffer(8);
                textureData.put(new double[]{NinjaCreeper.getPointX(841), NinjaCreeper.getPointY(0), NinjaCreeper.getPointX(841), NinjaCreeper.getPointY(19), NinjaCreeper.getPointX(954), NinjaCreeper.getPointY(19), NinjaCreeper.getPointX(954), NinjaCreeper.getPointY(0)});
                NinjaCreeper.draw2DQuadTextured(vertexData, textureData);

                glDisable(GL_TEXTURE_2D);
                glPopMatrix();
            }
        }

    }
}

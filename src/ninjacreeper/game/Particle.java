package ninjacreeper.game;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.util.Set;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.Color;

/**
 *
 * @author Matis
 */
public class Particle extends Entity {

    private float alpha = 1f;
    private final double fallAccel, lifeSpan;
    private double age = 0;
    private Color color;
    private final double groundCoord;

    public Particle(double fallAccel, double pos_x, double pos_y, double speed_x, double speed_y, double lifeSpan, Color color, EntityType entityType, double groundCoord) {
        super(pos_x, pos_y, entityType);
        this.fallAccel = fallAccel;
        this.speed_x = speed_x;
        this.speed_y = speed_y;
        this.lifeSpan = lifeSpan;
        this.color = color;
        this.groundCoord = groundCoord;
    }

    @Override
    public void draw() {
        if (entityType == EntityType.EXPLOSIONPARTICLE) {
            glPushMatrix();
            glTranslated(pos_x, pos_y, 0);
            glDisable(GL_TEXTURE_2D);
            DoubleBuffer vertexData = BufferUtils.createDoubleBuffer(8);
            vertexData.put(new double[]{0, 0, 0, entityType.getHeight(), entityType.getWidth(), entityType.getHeight(), entityType.getWidth(), 0});
            FloatBuffer colorData = BufferUtils.createFloatBuffer(16);
            colorData.put(new float[]{color.r, color.g, color.b, alpha, color.r, color.g, color.b, alpha, color.r, color.g, color.b, alpha, color.r, color.g, color.b, alpha});

            NinjaCreeper.draw2DQuadColored(vertexData, colorData);
            
            glPopMatrix();
            glDisableClientState(GL_COLOR_ARRAY);
            glDisableClientState(GL_VERTEX_ARRAY);
        } else {
            glPushMatrix();
            glTranslated(pos_x, pos_y, 0);

            glEnable(GL_TEXTURE_2D);
            DoubleBuffer vertexData = BufferUtils.createDoubleBuffer(8);
            vertexData.put(new double[]{0, 0, 0, entityType.getHeight() + Math.random() * 2, entityType.getWidth() + Math.random() * 2, entityType.getHeight() + Math.random() * 2, entityType.getWidth() + Math.random() * 2, 0});

            double coordX = NinjaCreeper.getPointX(GameScreen.level.tex_startX + 1);
            double coordY = NinjaCreeper.getPointY(groundCoord + 1);
            DoubleBuffer textureData = BufferUtils.createDoubleBuffer(8);
            textureData.put(new double[]{coordX, coordY, coordX, coordY, coordX, coordY, coordX, coordY});

            NinjaCreeper.draw2DQuadTextured(vertexData, textureData);
            glDisable(GL_TEXTURE_2D);
            glPopMatrix();
        }
        
        //  Color.white.bind();
    }

    public double getAge() {
        return age;
    }

    public double getLifeSpan() {
        return lifeSpan;
    }

    @Override
    public void physics() {
        speed_y += fallAccel;
    }

    @Override
    public void movements(double delta) {
        physics();
        moveEntity(delta);

        age += delta;
        alpha = 1f - (float) (age / lifeSpan);
    }

    @Override
    public void moveEntity(double delta) {
        double distanceToMoveHorizontally = speed_x * delta;
        double distanceToMoveVertically = speed_y * delta;

        while (distanceToMoveHorizontally > 0) {
            Set<Character> blocks = getVerticalBlocks(pos_x + entityType.getWidth(), pos_y, pos_y + entityType.getHeight());
            if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                pos_x += EntityType.MINVALUE;
            }
            distanceToMoveHorizontally -= EntityType.MINVALUE;
        }
        while (distanceToMoveHorizontally < 0) {
            Set<Character> blocks = getVerticalBlocks(pos_x - EntityType.MINVALUE, pos_y, pos_y + entityType.getHeight());
            if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                pos_x -= EntityType.MINVALUE;
            }
            distanceToMoveHorizontally += EntityType.MINVALUE;
        }
        while (distanceToMoveVertically < 0) {
            Set<Character> blocks = getHorizontalBlocks(pos_y, pos_x, pos_x + entityType.getWidth());
            if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                pos_y -= EntityType.MINVALUE;

            }
            distanceToMoveVertically += EntityType.MINVALUE;
        }
        while (distanceToMoveVertically > 0) {
            Set<Character> blocks = getHorizontalBlocks(pos_y + entityType.getHeight() + EntityType.MINVALUE, pos_x, pos_x + entityType.getWidth());
            if (!blocks.contains(Level.WALL) && !blocks.contains(Level.ICE) && !blocks.contains(Level.RUBBER)) {
                pos_y += EntityType.MINVALUE;
            }
            distanceToMoveVertically -= EntityType.MINVALUE;
        }
    }
}

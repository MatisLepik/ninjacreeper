package ninjacreeper.game;

import java.nio.DoubleBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Point;

/**
 *
 * @author Matis
 */
public class EndScreen extends GameState {

    private static double time;
    private static int levelNumber;
    private static int result;
    private static boolean isRecord;
    private static float scoreAlpha = 0f;
    private static final Point popup_topRight = new Point(0, 0), popup_topLeft = new Point(0, 0), popup_bottomRight = new Point(0, 0), popup_bottomLeft = new Point(0, 0);
    private static final Point popupFinal_topRight = new Point(NinjaCreeper.WIDTH / 2 + 159, NinjaCreeper.HEIGHT / 2 - 71);
    private static final Point popupFinal_topLeft = new Point(NinjaCreeper.WIDTH / 2 - 159, NinjaCreeper.HEIGHT / 2 - 71);
    private static final Point popupFinal_bottomRight = new Point(NinjaCreeper.WIDTH / 2 + 159, NinjaCreeper.HEIGHT / 2 + 72);
    private static final Point popupFinal_bottomLeft = new Point(NinjaCreeper.WIDTH / 2 - 159, NinjaCreeper.HEIGHT / 2 + 72);
    private static final float POPUPSPEED = 600;
    private static boolean isAnimationInProgress = false;
    private static boolean isAnimationsDone = true;

    @Override
    public void draw() {
        glPushMatrix();
        new Color(15 / 255f, 115 / 255f, 175 / 255f, 0.1f).bind();
        NinjaCreeper.game.draw();
        glPopMatrix();
        Color.white.bind();

        if (result == NinjaCreeper.RESULT_FINISH) {
            drawPopup(881, 1024);
            if (isRecord) {
                drawRecordIcon();
            }
            if (!isAnimationInProgress) {
                String timeString = "Your time: " + time + " sec.";
                AngelCodeFont font = NinjaCreeper.FONT_SMALL;
                //  glPushMatrix();
                glEnable(GL_BLEND);
                glEnable(GL_TEXTURE_2D);
                Color scoreCol = new Color(NinjaCreeper.COLOR_TEXT.r, NinjaCreeper.COLOR_TEXT.g, NinjaCreeper.COLOR_TEXT.b, scoreAlpha);
                font.drawString((NinjaCreeper.WIDTH / 2) - (font.getWidth(timeString) / 2), (NinjaCreeper.HEIGHT / 2) - (font.getHeight(timeString) / 2) + 15, timeString, scoreCol);
                NinjaCreeper.TEX_SPRITESHEET.bind();
                //  glPopMatrix();
            }
        } else {
            drawPopup(738, 881);

            String defeatString = "Unanticipated failure";
            if (result == NinjaCreeper.RESULT_FAIL_CAT) {
                defeatString = "AVOID THE CATS!";
            } else if (result == NinjaCreeper.RESULT_FAIL_LAZER) {
                defeatString = "YOU WERE SHOT BY A LAZER.";
            } else if (result == NinjaCreeper.RESULT_FAIL_LAVA) {
                defeatString = "DON'T TOUCH THE LAVA!";
            } else if (result == NinjaCreeper.RESULT_FAIL_OUTOFBOUNDS) {
                defeatString = "YOU FELL OUT OF THE WORLD.";
            }
            AngelCodeFont font = NinjaCreeper.FONT_SMALL;
            glEnable(GL_BLEND);
            glEnable(GL_TEXTURE_2D);
            Color scoreCol = new Color(NinjaCreeper.COLOR_TEXT.r, NinjaCreeper.COLOR_TEXT.g, NinjaCreeper.COLOR_TEXT.b, scoreAlpha);
            font.drawString((NinjaCreeper.WIDTH / 2) - (font.getWidth(defeatString) / 2), (NinjaCreeper.HEIGHT / 2) - (font.getHeight(defeatString) / 2) + 15, defeatString, scoreCol);
            NinjaCreeper.TEX_SPRITESHEET.bind();
        }
        
        glDisable(GL_BLEND);
        glDisable(GL_TEXTURE_2D);

    }

    @Override
    public void processInputs(double delta) {
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState() && isAnimationsDone) {
                NinjaCreeper.gameState = NinjaCreeper.menu;
                NinjaCreeper.isGameRunning = false;
            }
        }
        float verticalIncrease = 143 / POPUPSPEED;
        float horizontalIncrease = 318 / POPUPSPEED;

        popup_topRight.setY(Math.max(popupFinal_topRight.getY(), popup_topRight.getY() - verticalIncrease * (float) delta));
        popup_topLeft.setY(Math.max(popupFinal_topLeft.getY(), popup_topLeft.getY() - verticalIncrease * (float) delta));
        popup_bottomRight.setY(Math.min(popupFinal_bottomRight.getY(), popup_bottomRight.getY() + verticalIncrease * (float) delta));
        popup_bottomLeft.setY(Math.min(popupFinal_bottomLeft.getY(), popup_bottomLeft.getY() + verticalIncrease * (float) delta));
        popup_topRight.setX(Math.min(popupFinal_topRight.getX(), popup_topRight.getX() + horizontalIncrease * (float) delta));
        popup_topLeft.setX(Math.max(popupFinal_topLeft.getX(), popup_topLeft.getX() - horizontalIncrease * (float) delta));
        popup_bottomRight.setX(Math.min(popupFinal_bottomRight.getX(), popup_bottomRight.getX() + horizontalIncrease * (float) delta));
        popup_bottomLeft.setX(Math.max(popupFinal_bottomLeft.getX(), popup_bottomLeft.getX() - horizontalIncrease * (float) delta));

        if (popupFinal_topRight.getY() == popup_topRight.getY()) {
            isAnimationInProgress = false;
            scoreAlpha = Math.min(1f, scoreAlpha + (POPUPSPEED / 100000) * (float) delta);

            if (scoreAlpha == 1f) {
                isAnimationsDone = true;
            }
        }
    }

    public static void setScreen(int _result, double _time, int _levelNumber, boolean _isRecord) {
        result = _result;
        time = _time;
        levelNumber = _levelNumber;
        isRecord = _isRecord;
    }

    public static void popAnimation() {
        popup_topRight.setY(NinjaCreeper.HEIGHT / 2);
        popup_topLeft.setY(NinjaCreeper.HEIGHT / 2);
        popup_bottomRight.setY(NinjaCreeper.HEIGHT / 2);
        popup_bottomLeft.setY(NinjaCreeper.HEIGHT / 2);
        popup_topRight.setX(NinjaCreeper.WIDTH / 2);
        popup_topLeft.setX(NinjaCreeper.WIDTH / 2);
        popup_bottomRight.setX(NinjaCreeper.WIDTH / 2);
        popup_bottomLeft.setX(NinjaCreeper.WIDTH / 2);
        isAnimationInProgress = true;
        isAnimationsDone = false;
        scoreAlpha = -0.5f;
    }

    private void drawPopup(double xMin, double xMax) {
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);

        DoubleBuffer vertexData = BufferUtils.createDoubleBuffer(8);
        vertexData.put(new double[]{popup_topLeft.getX(), popup_topLeft.getY(), popup_bottomLeft.getX(), popup_bottomLeft.getY(), popup_bottomRight.getX(), popup_bottomRight.getY(), popup_topRight.getX(), popup_topRight.getY()});

        DoubleBuffer textureData = BufferUtils.createDoubleBuffer(8);
        textureData.put(new double[]{NinjaCreeper.getPointX(xMax), NinjaCreeper.getPointY(194), NinjaCreeper.getPointX(xMin), NinjaCreeper.getPointY(194), NinjaCreeper.getPointX(xMin), NinjaCreeper.getPointY(512), NinjaCreeper.getPointX(xMax), NinjaCreeper.getPointY(512)});

        NinjaCreeper.draw2DQuadTextured(vertexData, textureData);

        glPopMatrix();
    }

    private void drawRecordIcon() {

    }

}

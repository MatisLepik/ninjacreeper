package ninjacreeper.game;

/**
 *
 * @author Matis
 */
public enum EntityType {

    PLAYER(700, //textureX
            129, //textureY
            24, //width
            36, //height
            0.22, //moveSpeed
            0.45, //jumpSpeed
            0.02, //fallAccel
            0.03, //moveAccel
            100), //move animation speed
    FINISH(700, //textureX
            187, //textureY
            16, //width
            50, //height
            0.15, //moveSpeed
            0.45, //jumpSpeed
            0.02, //fallAccel
            0.02, //moveAccel
            100), //move animation speed
    ENEMY(700, //textureX
            166, //textureY
            38, //width
            20, //height
            0.1, //moveSpeed
            0.45, //jumpSpeed
            0.02, //fallAccel
            0.015, //moveAccel
            100), //move animation speed
    EXPLOSIONPARTICLE(0, //textureX
            0, //textureY
            2, //width
            1, //height
            0, //moveSpeed
            0, //jumpSpeed
            0, //fallAccel
            0, //moveAccel
            0), //move animation speed
    SPEEDPARTICLE(0, //textureX
            0, //textureY
            2, //width
            1, //height
            0, //moveSpeed
            0, //jumpSpeed
            0, //fallAccel
            0, //moveAccel
            0), //move animation speed
    SLIDEPARTICLE(0, //textureX
            0, //textureY
            1, //width
            1, //height
            0, //moveSpeed
            0, //jumpSpeed
            0, //fallAccel
            0, //moveAccel
            0), //move animation speed
    GUNPOWDER(700, //textureX
            291, //textureY
            12, //width
            10, //height
            0, //moveSpeed
            0, //jumpSpeed
            0, //fallAccel
            0, //moveAccel
            0), //move animation speed
    LAZERBEAM_VERT(0, //textureX
            0, //textureY
            2, //width
            12, //height
            0, //moveSpeed
            0, //jumpSpeed
            0, //fallAccel
            0, //moveAccel
            0), //move animation speed
    LAZERBEAM_HOR(0, //textureX
            0, //textureY
            12, //width
            2, //height
            0, //moveSpeed
            0, //jumpSpeed
            0, //fallAccel
            0, //moveAccel
            0); //move animation speed

    private final double textureX, textureY, width, height, moveSpeed, jumpSpeed, fallAccel, moveAccel, moveAnimationSpeed;
    public static double MINVALUE = 0.1;

    private EntityType(double textureX, double textureY, double width, double height, double moveSpeed, double jumpSpeed, double fallAccel, double moveAccel, double moveAnimationSpeed) {
        this.textureX = textureX;
        this.textureY = textureY;
        this.width = width;
        this.height = height;
        this.moveSpeed = moveSpeed;
        this.jumpSpeed = jumpSpeed;
        this.fallAccel = fallAccel;
        this.moveAccel = moveAccel;
        this.moveAnimationSpeed = moveAnimationSpeed;
    }

    public double getTextureX(boolean isRunningAgainstWall, double speed, double runningAnimationCount) {
        if (speed != 0) {
            return textureX + width * runningAnimationCount + runningAnimationCount;

        } else {
            if (isRunningAgainstWall) {
                return textureX + width * 4 + 4;
            } else {
                return textureX;
            }
        }
    }

    public double getTextureY() {
        return textureY;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getFallAccel() {
        return fallAccel;
    }

    public double getJumpSpeed() {
        return jumpSpeed;
    }

    public double getMoveSpeed() {
        return moveSpeed;
    }

    public double getMoveAnimationSpeed(double speedX) {
        double speed = Math.abs(speedX);
        if (speed > moveSpeed) {
            return moveAnimationSpeed - (moveAnimationSpeed*0.3);
        } else if (speed < moveSpeed) {
            return moveAnimationSpeed * 2;
        } else {
            return moveAnimationSpeed;
        }
        
    }

    public double getMoveAccel() {
        return moveAccel;
    }

    public double getRubberJumpBoost() {
        return -0.1;
    }

    public double getCrouchSize() {
        return 30;
    }

    public double getWallJumpSpeed_vertical() {
        return jumpSpeed;
    }

    public double getWallJumpSpeed_horizontal() {
        return 0.5;
    }

    public double getIceSpeed() {
        return moveSpeed * 1.5;
    }

    public double getIceFriction() {
        return 0.008;
    }

    public double getWallJumpSpeed() {
        return moveSpeed * 2;
    }
    
    public double getSprintSpeedBoost() {
        return -0.2;
    }
    
    public double getMaxFallSpeed() {
        return 1;
    }
}

package ninjacreeper.game;

/**
 *
 * @author Matis
 */
public class Gunpowder extends Entity {

    private final double MAXELEVATION;
    private double elevated = 3;
    private boolean elevating = true;

    public Gunpowder(double pos_x, double pos_y) {
        super(pos_x, pos_y, EntityType.GUNPOWDER);
        MAXELEVATION = 8 + Math.random() * 4;
    }

    @Override
    public void physics() {
        if (elevating) {
            if (elevated < MAXELEVATION) {
                speed_y = Math.max(speed_y - 0.0002, -0.01);
            } else {
                speed_y = 0;
                elevating = false;
            }
        } else {
            if (elevated > 0) {
                speed_y = Math.min(speed_y + 0.0002, 0.01);
            } else {
                speed_y = 0;
                elevating = true;
            }
        }
    }

    @Override
    public void movements(double delta) {
        physics();
        double distanceToMove = speed_y * delta;
        elevated += -distanceToMove;
        pos_y += distanceToMove;
    }
}


package ninjacreeper.game;

import java.util.Set;

/**
 *
 * @author Matis
 */
public class Enemy extends Entity {
    
    private boolean isEnemyOnLava = false;
    
    public Enemy(double pos_x, double pos_y) {
        super(pos_x, pos_y, EntityType.ENEMY);
        speed_x = -entityType.getMoveSpeed();
    }
    
    @Override
    public void physics() {
        Set<Character> blocksBelow = getHorizontalBlocks(pos_y + entityType.getHeight() + EntityType.MINVALUE, pos_x, pos_x + entityType.getWidth());
        Set<Character> blocksRight = getVerticalBlocks(pos_x + entityType.getWidth() + EntityType.MINVALUE, pos_y, pos_y + entityType.getHeight());
        Set<Character> blocksLeft = getVerticalBlocks(pos_x - EntityType.MINVALUE, pos_y, pos_y + entityType.getHeight());
        boolean againstRightWall = blocksRight.contains(Level.WALL) || blocksRight.contains(Level.HAZARD) || blocksRight.contains(Level.ICE);
        boolean againstLeftWall = blocksLeft.contains(Level.WALL) || blocksRight.contains(Level.HAZARD) || blocksRight.contains(Level.ICE);
        boolean inAir = !blocksBelow.contains(Level.RUBBER) && !blocksBelow.contains(Level.ICE) && !blocksBelow.contains(Level.WALL);
        isEnemyOnLava = blocksBelow.contains(Level.HAZARD) || blocksLeft.contains(Level.HAZARD) || blocksRight.contains(Level.HAZARD);
        //GRAVITY
        gravity(inAir, blocksBelow);
        
        // MOVEMENTS
        if (againstLeftWall || againstRightWall) {
            speed_x = 0;
            facingLeft = !facingLeft;
        } else if (blocksLeft.contains(Level.RUBBER) || blocksRight.contains(Level.RUBBER)) {
            speed_x = 0;
            return;
        }
        
        if (facingLeft) {
            speed_x = Math.max(speed_x - entityType.getMoveAccel(), -entityType.getMoveSpeed());
        } else {
            speed_x = Math.min(speed_x + entityType.getMoveAccel(), entityType.getMoveSpeed());
        }
        movementAnimation(inAir);
    }    
    
    public boolean isEnemyOnLava() {
        return isEnemyOnLava;
    }
}

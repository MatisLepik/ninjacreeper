package ninjacreeper.game;

import java.awt.geom.Point2D;

/**
 *
 * @author Matis
 */
public enum TurretDirection {

    UP(0, -0.25, 16, 10, new Point2D.Double(700, 290), new Point2D.Double(700, 274), new Point2D.Double(710, 290), new Point2D.Double(710, 274), 6, -EntityType.LAZERBEAM_VERT.getHeight()),
    DOWN(0, 0.25, 16, 10, new Point2D.Double(710, 274), new Point2D.Double(710, 290), new Point2D.Double(700, 274), new Point2D.Double(700, 290), 6, 13),
    LEFT(-0.25, 0, 10, 16, new Point2D.Double(700, 274), new Point2D.Double(710, 274), new Point2D.Double(700, 290), new Point2D.Double(710, 290), 3-EntityType.LAZERBEAM_HOR.getWidth(), 6),
    RIGHT(0.25, 0, 10, 16, new Point2D.Double(710, 274), new Point2D.Double(700, 274), new Point2D.Double(710, 290), new Point2D.Double(700, 290), 7, 6);

    private final double speed_x, speed_y, width, height, bulletSpawn_x, bulletSpawn_y;
    private final Point2D tex_topLeft, tex_topRight, tex_bottomLeft, tex_bottomRight;

    private TurretDirection(double speed_x, double speed_y, double width, double height, Point2D tex_topLeft, Point2D tex_topRight, Point2D tex_bottomLeft, Point2D tex_bottomRight, double bulletSpawn_x, double bulletSpawn_y) {
        this.speed_x = speed_x;
        this.speed_y = speed_y;
        this.width = width;
        this.height = height;
        this.tex_bottomLeft = tex_bottomLeft;
        this.tex_bottomRight = tex_bottomRight;
        this.tex_topLeft = tex_topLeft;
        this.tex_topRight = tex_topRight;
        this.bulletSpawn_x = bulletSpawn_x;
        this.bulletSpawn_y = bulletSpawn_y;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getSpeedX() {
        return speed_x;
    }

    public double getSpeedY() {
        return speed_y;
    }

    public Point2D getTopLeft() {
        return get2DPoint(tex_topLeft);
    }

    public Point2D getTopRight() {
        return get2DPoint(tex_topRight);
    }

    public Point2D getBottomLeft() {
        return get2DPoint(tex_bottomLeft);
    }

    public Point2D getBottomRight() {
        return get2DPoint(tex_bottomRight);
    }

    public double getBulletSpawnX() {
        return bulletSpawn_x;
    }

    public double getBulletSpawnY() {
        return bulletSpawn_y;
    }

    private Point2D get2DPoint(Point2D pixelPoint) {
        return new Point2D.Double(NinjaCreeper.getPointX(pixelPoint.getX()), NinjaCreeper.getPointY(pixelPoint.getY()));
    }

    public static TurretDirection getFacing(double facing) {
        if (facing == Turret.TURRETFACE_DOWN) {
            return DOWN;
        } else if (facing == Turret.TURRETFACE_UP) {
            return UP;
        } else if (facing == Turret.TURRETFACE_LEFT) {
            return LEFT;
        } else {
            return RIGHT;
        }
    }

}

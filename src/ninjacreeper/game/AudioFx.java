
package ninjacreeper.game;

/**
 *
 * @author Matis
 */
public enum AudioFx {
    STEP("step.wav"), JUMPBOOST("jumpboost.wav"), LAZER("lazer.wav"), LAZERIMPACT("lazerimpact.wav"), PICKUP("pickup.wav"), WIN("win.wav"), LOSE("lose.wav"), BUTTONCLICK("click.wav"), MOUSEOVER("mouseover.wav");
    
    private final String RESOURCEFOLDER = "/ninjacreeper/resources/audio/";
    private final String path;
    
    private AudioFx(String path) {
        this.path = path;
    }

    public String getPath() {
        return RESOURCEFOLDER + path;
    }
    
}

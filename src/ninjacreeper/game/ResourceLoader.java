package ninjacreeper.game;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

/**
 *
 * @author Matis
 */
public class ResourceLoader {

    @SuppressWarnings("unchecked")
    public static boolean loadDefaultFiles(int number) {
        double playerX, playerY, finishX, finishY;
        ArrayList<Point2D> enemyPositions, gunpowderPositions;
        ArrayList<double[]> turretData;
        char[][] blockData;
        Texture backgroundImage, foregroundImage = null;

        try {
            InputStream IS = NinjaCreeper.class
                    .getResourceAsStream("/ninjacreeper/resources/defaultlevels/level" + number + "/ent1.dat");
            ObjectInputStream input = new ObjectInputStream(IS);

            double[] coordArray = (double[]) input.readObject();
            playerX = coordArray[0];
            playerY = coordArray[1];
            finishX = coordArray[2];
            finishY = coordArray[3];
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
        try {
            InputStream IS = NinjaCreeper.class
                    .getResourceAsStream("/ninjacreeper/resources/defaultlevels/level" + number + "/ent2.dat");
            ObjectInputStream input = new ObjectInputStream(IS);
            enemyPositions = (ArrayList<Point2D>) input.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
        try {
            InputStream IS = NinjaCreeper.class
                    .getResourceAsStream("/ninjacreeper/resources/defaultlevels/level" + number + "/ent3.dat");
            ObjectInputStream input = new ObjectInputStream(IS);
            turretData = (ArrayList<double[]>) input.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
        try {
            InputStream IS = NinjaCreeper.class
                    .getResourceAsStream("/ninjacreeper/resources/defaultlevels/level" + number + "/ent4.dat");
            ObjectInputStream input = new ObjectInputStream(IS);
            gunpowderPositions = (ArrayList<Point2D>) input.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
        try {
            InputStream IS = NinjaCreeper.class
                    .getResourceAsStream("/ninjacreeper/resources/defaultlevels/level" + number + "/blocks.dat");
            ObjectInputStream input = new ObjectInputStream(IS);
            blockData = (char[][]) input.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
        backgroundImage = NinjaCreeper.getTexture("/ninjacreeper/resources/defaultlevels/level" + number + "/background.png");
        foregroundImage = NinjaCreeper.getTexture("/ninjacreeper/resources/defaultlevels/level" + number + "/foreground.png");
        
        GameScreen.setUpGame(playerX, playerY, finishX, finishY, enemyPositions, gunpowderPositions, turretData, blockData, backgroundImage, foregroundImage, number);
        return true;
    }

    @SuppressWarnings("unchecked")
    public static boolean loadCustomFiles(File directory) {
        double playerX, playerY, finishX, finishY;
        ArrayList<Point2D> enemyPositions, gunpowderPositions;
        char[][] blockData;
        ArrayList<double[]> turretData;
        Texture backgroundImage, foregroundImage = null;

        try {
            File file = new File(directory, "ent1.dat");
            FileInputStream fileStream = new FileInputStream(file);
            ObjectInputStream input = new ObjectInputStream(fileStream);

            double[] coordArray = (double[]) input.readObject();
            playerX = coordArray[0];
            playerY = coordArray[1];
            finishX = coordArray[2];
            finishY = coordArray[3];
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
        try {
            File file = new File(directory, "ent2.dat");
            FileInputStream fileStream = new FileInputStream(file);
            ObjectInputStream input = new ObjectInputStream(fileStream);

            enemyPositions = (ArrayList<Point2D>) input.readObject();

        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
        try {
            File file = new File(directory, "ent3.dat");
            FileInputStream fileStream = new FileInputStream(file);
            ObjectInputStream input = new ObjectInputStream(fileStream);

            turretData = (ArrayList<double[]>) input.readObject();

        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
        try {
            File file = new File(directory, "ent4.dat");
            FileInputStream fileStream = new FileInputStream(file);
            ObjectInputStream input = new ObjectInputStream(fileStream);

            gunpowderPositions = (ArrayList<Point2D>) input.readObject();

        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
        try {
            File file = new File(directory, "blocks.dat");
            FileInputStream fileStream = new FileInputStream(file);
            ObjectInputStream input = new ObjectInputStream(fileStream);

            blockData = (char[][]) input.readObject();

        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
        File backgroundImg = new File(directory, "background.png");
        if (backgroundImg.exists()) {
            try {
                backgroundImage = TextureLoader.getTexture("PNG", new FileInputStream(backgroundImg));
            } catch (IOException ex) {
                ex.printStackTrace();
                return false;
            }
        } else {
            backgroundImage = NinjaCreeper.getTexture("/ninjacreeper/resources/images/background.png");
        }

        File foregroundImg = new File(directory, "foreground.png");
        if (foregroundImg.exists()) {
            try {
                foregroundImage = TextureLoader.getTexture("PNG", new FileInputStream(foregroundImg));
            } catch (IOException ex) {
                ex.printStackTrace();
                return false;
            }
        }

        GameScreen.setUpGame(playerX, playerY, finishX, finishY, enemyPositions, gunpowderPositions, turretData, blockData, backgroundImage, foregroundImage, 0);
        return true;
    }

}

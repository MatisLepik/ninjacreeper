package ninjacreeper.menu;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import ninjacreeper.game.NinjaCreeper;
import org.newdawn.easyogg.OggClip;

/**
 *
 * @author Matis
 */
public class MusicPlayer {

    private static int musicVolume = (int) NinjaCreeper.options[NinjaCreeper.OPT_MUSIC_VOLUME];
    private static boolean isMusicMuted = NinjaCreeper.options[NinjaCreeper.OPT_MUSIC_MUTED] == 1;

    private static int lastSong = 0;

    static ArrayList<InputStream> songs;

    private static OggClip music;

    public static void startPlayer() {
        try {
            InputStream in = randomSong();
            music = new OggClip(in);
            music.setGain((float) musicVolume / 100);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        playSong();
        refreshMusicMuted();
    }

    public static void playSong() {
        music.play();
    }

    public static void refreshMusicVolume() {
        musicVolume = (int) NinjaCreeper.options[NinjaCreeper.OPT_MUSIC_VOLUME];
        if (isMusicMuted) {
            music.resume();
        }
        music.setGain((float) musicVolume / 100);
        if (isMusicMuted) {
            music.pause();
        }
    }

    public static int getMusicVolume() {
        refreshMusicVolume();
        return musicVolume;
    }

    public static void refreshMusicMuted() {
        if (NinjaCreeper.options[NinjaCreeper.OPT_MUSIC_MUTED] == 1) {
            if (!music.isPaused()) {
                music.pause();
            }
            isMusicMuted = true;
        } else {
            if (music.isPaused()) {
                music.resume();
            }
            isMusicMuted = false;
        }
    }

    public static boolean getMusicMuted() {
        refreshMusicMuted();
        return isMusicMuted;
    }

    private static InputStream randomSong() {
        int newSong = 1 + (int) (Math.random() * 4);
        while (newSong == lastSong) {
            newSong = 1 + (int) (Math.random() * 4);
        }
        lastSong = newSong;

        if (newSong == 1) {
            return NinjaCreeper.class.getResourceAsStream(Songs.FRACTION.getLocation());
        }
        if (newSong == 2) {
            return NinjaCreeper.class.getResourceAsStream(Songs.FUNKBLASTER.getLocation());
        }
        if (newSong == 3) {
            return NinjaCreeper.class.getResourceAsStream(Songs.ICESKATING.getLocation());
        }
        if (newSong == 4) {
            return NinjaCreeper.class.getResourceAsStream(Songs.PYSEN.getLocation());
        } else {
            return null;
        }
    }

    public static void checkSongStatus() {
        if (music.stopped()) {
            startPlayer();
        }
    }

}

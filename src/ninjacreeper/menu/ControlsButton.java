package ninjacreeper.menu;

import ninjacreeper.game.AudioFx;
import ninjacreeper.game.FxPlayer;
import ninjacreeper.game.NinjaCreeper;
import org.lwjgl.input.Keyboard;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Matis
 */
public class ControlsButton extends GUIComponent {

    private final int key;
    private boolean isSelectingKey = false;  

    public ControlsButton(double x, double y, double width, double height, int key) {
        super(x, y, width, height, false);
        this.key = key;
        color = NinjaCreeper.COLOR_TEXT;
        font = NinjaCreeper.FONT_SMALL;
    }

    @Override
    public void draw() {
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glTranslated(getX(), getY(), 0);
        
        String text;
        if (isSelectingKey) {
            text = "...";
        } else {
            text = Keyboard.getKeyName((int)NinjaCreeper.options[key]);
        }
        while (font.getWidth(text) >= getWidth()) {
            text = text.substring(0, text.length() - 1);
        }
        font.drawString(((float)getWidth() / 2f) - (font.getWidth(text) / 2) - 1, ((float)getHeight() / 2f) - (font.getHeight(text) / 2) - 2, text, color);
        NinjaCreeper.TEX_SPRITESHEET.bind();
        glPopMatrix();
    }

    @Override
    public void mousePressed() {
        isSelectingKey = true;
        FxPlayer.playSound(AudioFx.BUTTONCLICK);
    }
   

    public void keyPressed(int eventKey) {
        if (isSelectingKey) {
            NinjaCreeper.options[key] = eventKey;
            isSelectingKey = false;
        }
    }

    public void tryCancelSelection() {
        isSelectingKey = false;
    }

}

package ninjacreeper.menu;

import ninjacreeper.game.AudioFx;
import ninjacreeper.game.FxPlayer;
import ninjacreeper.game.NinjaCreeper;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.Color;

/**
 *
 * @author Matis
 */
public class SpeakerButton extends GUIComponent {

    private final VolumeType volumeType;

    public SpeakerButton(float x, float y, VolumeType volumeType) {
        super(x, y, 14, 12, true);
        this.volumeType = volumeType;
        font = NinjaCreeper.FONT_SMALL;
        setTextureX_min(700);
        setTextureX_max(714);
    }

    @Override
    public void draw() {
        switch (volumeType) {
            case MUSIC:
                setTexture(MusicPlayer.getMusicMuted());
                break;
            case FX:
                setTexture(FxPlayer.getFxMuted());
                break;
        }
        glPushMatrix();
        color.bind();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glTranslated(getX(), getY(), 0);
        glBegin(GL_QUADS);

        glTexCoord2d(getPointX_min(), getPointY_min());// Top left
        glVertex2d(0, 0); // Top left

        glTexCoord2d(getPointX_min(), getPointY_max()); // Bottom left
        glVertex2d(0, getHeight()); // Bottom left

        glTexCoord2d(getPointX_max(), getPointY_max());// Bottom right
        glVertex2d(getWidth(), getHeight()); // Bottom right

        glTexCoord2d(getPointX_max(), getPointY_min());// Top right
        glVertex2d(getWidth(), 0); // Top right
        
        glEnd();
        glPopMatrix();
        Color.white.bind();
    }
    
    @Override
    public void mousePressed() {
        toggleMuted();
        FxPlayer.playSound(AudioFx.BUTTONCLICK);
    }

    
    private void toggleMuted() {
        switch (volumeType) {
            case FX:
                if (NinjaCreeper.options[NinjaCreeper.OPT_FX_MUTED] == 1) {
                    NinjaCreeper.options[NinjaCreeper.OPT_FX_MUTED] = 0;
                } else {
                    NinjaCreeper.options[NinjaCreeper.OPT_FX_MUTED] = 1;
                }
                FxPlayer.refreshFxMuted();
                break;
            case MUSIC:
                if (NinjaCreeper.options[NinjaCreeper.OPT_MUSIC_MUTED] == 1) {
                    NinjaCreeper.options[NinjaCreeper.OPT_MUSIC_MUTED] = 0;
                } else {
                    NinjaCreeper.options[NinjaCreeper.OPT_MUSIC_MUTED] = 1;
                }
                MusicPlayer.refreshMusicMuted();
                break;
        }
    }

    private void setTexture(boolean isMuted) {
        if (isMuted) {
            setTextureY_max(105);
            setTextureY_min(117);
        } else {
            setTextureY_max(117);
            setTextureY_min(129);
        }
    }

}

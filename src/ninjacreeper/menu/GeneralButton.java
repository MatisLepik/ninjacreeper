
package ninjacreeper.menu;

import ninjacreeper.game.AudioFx;
import ninjacreeper.game.FxPlayer;
import ninjacreeper.game.NinjaCreeper;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Matis
 */
public class GeneralButton extends GUIComponent {
    
    private final String title;
    
    public GeneralButton(double x, double y, double width, double height, String title) {
        super(x, y, width, height, false);
        this.title = title;
        font = NinjaCreeper.FONT_SMALL;
    }
    
    @Override
    public void draw() {
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glTranslated(getX(), getY(), 0);
        
        font.drawString(((float)getWidth() / 2f) - (font.getWidth(title) / 2) - 1, ((float)getHeight() / 2f) - (font.getHeight(title) / 2) - 2, title, color);
        NinjaCreeper.TEX_SPRITESHEET.bind();
        glPopMatrix();
    }
    
    @Override
    public void mousePressed() {
        FxPlayer.playSound(AudioFx.BUTTONCLICK);
    }
    
}

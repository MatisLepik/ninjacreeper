package ninjacreeper.menu;

import ninjacreeper.game.AudioFx;
import ninjacreeper.game.FxPlayer;
import ninjacreeper.game.NinjaCreeper;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Color;

/**
 *
 * @author Matis
 */
public class GUIComponent {

    private double textureX_min = 1, textureX_max = 1, textureY_min = 1, textureY_max = 1; 
    private double x, y, width, height;
    boolean mousePressed, mouseOvered;
    private boolean usingTexture = false;
    private String text;
    public Color color = NinjaCreeper.COLOR_TEXT;
    public AngelCodeFont font;

    public GUIComponent(double x, double y, double width, double height, boolean usingTexture) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.usingTexture = usingTexture;
    }

    public void draw() {
        if (usingTexture) {
            glPushMatrix();
            glEnable(GL_TEXTURE_2D);
            glEnable(GL_BLEND);
            glTranslated(getX(), getY(), 0);
            
            glBegin(GL_QUADS);

            glTexCoord2d(textureX_min, textureY_min);// Top left
            glVertex2d(0, 0); // Top left

            glTexCoord2d(textureX_min, textureY_max); // Bottom left
            glVertex2d(0, height); // Bottom left

            glTexCoord2d(textureX_max, textureY_max);// Bottom right
            glVertex2d(width, height); // Bottom right

            glTexCoord2d(textureX_max, textureY_min);// Top right
            glVertex2d(width, 0); // Top right

            glEnd();
            
            glPopMatrix();
        }
    }
    
    public void setUsingTexture(boolean input) {
        usingTexture = input;
    }
    
    public double getPointX_min() {
        return textureX_min;
    }

    public double getPointX_max() {
        return textureX_max;
    }

    public double getPointY_min() {
        return textureY_min;
    }

    public double getPointY_max() {
        return textureY_max;
    }

    public void setTextureX_min(double input) {
        textureX_min = NinjaCreeper.getPointX(input);
    }

    public void setTextureX_max(double input) {
        textureX_max = NinjaCreeper.getPointX(input);
    }

    public void setTextureY_min(double input) {
        textureY_min = NinjaCreeper.getPointY(input);
    }

    public void setTextureY_max(double input) {
        textureY_max = NinjaCreeper.getPointY(input);
    }

    public void mouseWithin() {
        color = NinjaCreeper.COLOR_HIGHLIGHT;
        if (!mouseOvered) {
            FxPlayer.playSound(AudioFx.MOUSEOVER);
            mouseOvered = true;
        }
    }

    public void mouseNotWithin() {
        color = NinjaCreeper.COLOR_TEXT;
        mouseOvered = false;
    }

    public void mousePressed() {

    }

    public void mouseReleased() {

    }

    public boolean inBounds() {
        return NinjaCreeper.getMouseX() > this.x && NinjaCreeper.getMouseX() < this.x + this.width && NinjaCreeper.getMouseY() > this.y && NinjaCreeper.getMouseY() < this.y + height;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getText() {
        return text;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getWidth() {
        return this.width;
    }

    public double getHeight() {
        return this.height;
    }

}

package ninjacreeper.menu;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import javax.swing.JFileChooser;
import ninjacreeper.game.*;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Color;

/**
 *
 * @author Matis
 */
public class MenuScreen extends GameState {

    private final AngelCodeFont font;

    GUIComponent GUI_background = new GUIComponent(51, 0, 700, 500, true);
    SpeakerButton musicSpeaker = new SpeakerButton(83, 406, VolumeType.MUSIC);
    SpeakerButton fxSpeaker = new SpeakerButton(83, 435, VolumeType.FX);
    VolumeSlider musicSlider = new VolumeSlider(106, 400, 100, 24, VolumeType.MUSIC);
    VolumeSlider fxSlider = new VolumeSlider(106, 429, 100, 24, VolumeType.FX);
    GeneralButton startGameButton = new GeneralButton(350, 400, 98, 24, "SELECT LEVEL");
    GeneralButton customGameButton = new GeneralButton(350, 429, 98, 24, "CUSTOM GAME");
    ControlsButton con_up = new ControlsButton(633, 391f, 40f, 20, NinjaCreeper.OPT_UP);
    ControlsButton con_left = new ControlsButton(588, 416, 40, 20, NinjaCreeper.OPT_LEFT);
    ControlsButton con_down = new ControlsButton(633, 416, 40, 20, NinjaCreeper.OPT_DOWN);
    ControlsButton con_right = new ControlsButton(678, 416, 40, 20, NinjaCreeper.OPT_RIGHT);
    ControlsButton con_leap = new ControlsButton(588, 441, 130, 20, NinjaCreeper.OPT_LEAP);

    LevelPanel level1 = new LevelPanel(76, 117, 1);
    LevelPanel level2 = new LevelPanel(76, 251, 2);
    LevelPanel level3 = new LevelPanel(245, 251, 3);
    LevelPanel level4 = new LevelPanel(245, 117, 4);
    LevelPanel level5 = new LevelPanel(414, 117, 5);
    LevelPanel level6 = new LevelPanel(414, 251, 6);
    LevelPanel level7 = new LevelPanel(583, 251, 7);
    LevelPanel level8 = new LevelPanel(583, 117, 8);

    int shaderProgram = 0;

    private long lastClick = 0;

    ArrayList<ControlsButton> controlButtons = new ArrayList<>(Arrays.asList(con_up, con_left, con_down, con_right, con_leap));
    ArrayList<LevelPanel> levelPanels = new ArrayList<>(Arrays.asList(level1, level2, level3, level4, level5, level6, level7, level8));

    public MenuScreen() {
        font = NinjaCreeper.FONT_MEDIUM;
        GUI_background.setTextureX_max(700);
        GUI_background.setTextureY_max(500);
        GUI_background.setTextureX_min(0);
        GUI_background.setTextureY_min(0);
    }

    @Override
    public void draw() {
        glDisable(GL_BLEND);
        glDisable(GL_TEXTURE_2D);
        
        new Color(15 / 255f, 115 / 255f, 175 / 255f, 0.9f).bind();
        glRectd(0, 0, NinjaCreeper.WIDTH, NinjaCreeper.HEIGHT);

        if (NinjaCreeper.isGameRunning) {
            NinjaCreeper.game.draw();
        } else {
//            Color.white.bind();
        }

        Color.white.bind();
        GUI_background.draw();
        drawKeyControls();
        //drawLevelNames();
        drawLevelPanels();
        drawGeneralButtons();
        drawSpeakerButtons();
    }

    @Override
    public void processInputs(double delta
    ) {
        while (Keyboard.next()) {
            controlsButton_keyEvents();
        }
        while (Mouse.next()) {
            speakerButtons_mouseEvents();
            sliderButtons_mouseEvents();
            controlsButton_mouseEvents();
            levelPanels_mouseEvents();
            startGameButton_mouseEvents();
            customGameButton_mouseEvents();
        }

    }

    private void sliderButtons_mouseEvents() {
        if (musicSlider.inBounds()) {
            musicSlider.mouseWithin();
            if (Mouse.isButtonDown(0)) {
                musicSlider.setVolume((int) musicSlider.getDistanceFromX());
            }
        } else {
            musicSlider.mouseNotWithin();
            if (fxSlider.inBounds()) {
                fxSlider.mouseWithin();
                if (Mouse.isButtonDown(0)) {
                    fxSlider.setVolume((int) fxSlider.getDistanceFromX());
                }
            } else {
                fxSlider.mouseNotWithin();
            }
        }
        if (musicSlider.inBounds() || fxSlider.inBounds()) {
            if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()) {
                FxPlayer.playSound(AudioFx.BUTTONCLICK);
            }
        }
    }

    private void speakerButtons_mouseEvents() {
        if (fxSpeaker.inBounds()) {
            fxSpeaker.mouseWithin();
            if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()) {
                fxSpeaker.mousePressed();
            }
        } else {
            fxSpeaker.mouseNotWithin();
            if (musicSpeaker.inBounds()) {
                musicSpeaker.mouseWithin();
                if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()) {
                    musicSpeaker.mousePressed();
                }
            } else {
                musicSpeaker.mouseNotWithin();
            }
        }
    }

    private void controlsButton_keyEvents() {
        if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            for (ControlsButton c : controlButtons) {
                c.tryCancelSelection();
            }
            NinjaCreeper.saveOptions();
            if (NinjaCreeper.isGameRunning) {
                NinjaCreeper.gameState = NinjaCreeper.game;
            }
        } else if (Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
            NinjaCreeper.startGame(LevelPanel.getSelectedLevel(), null);
        } else {
            for (ControlsButton c : controlButtons) {
                c.keyPressed(Keyboard.getEventKey());
            }
        }
    }

    private void controlsButton_mouseEvents() {
        for (ControlsButton button : controlButtons) {
            if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()) {
                button.tryCancelSelection();
            }
            if (button.inBounds()) {
                button.mouseWithin();
                if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()) {
                    button.mousePressed();
                }
            } else {
                button.mouseNotWithin();
            }
        }
    }

    private void levelPanels_mouseEvents() {
        for (LevelPanel panel : levelPanels) {
            if (panel.inBounds()) {
                panel.mouseWithin();

                if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState() && panel.getLevel() == LevelPanel.getSelectedLevel() && NinjaCreeper.getTime() - lastClick < 250) {
                    lastClick = NinjaCreeper.getTime();
                    NinjaCreeper.startGame(LevelPanel.getSelectedLevel(), null);
                } else if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()) {
                    panel.mousePressed();
                    lastClick = NinjaCreeper.getTime();
                }

            } else {
                panel.mouseNotWithin();
            }
        }
    }

    private void drawLevelNames() {
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);

        String stage1 = "Winter is coming";
        String stage2 = "The climb";
        String stage3 = "The Night is Dark";
        String stage4 = "Fire and blood";
        font.drawString(144 - font.getWidth(stage1) / 2, 227, stage1, NinjaCreeper.COLOR_HIGHLIGHT);
        font.drawString(313 - font.getWidth(stage2) / 2, 227, stage2, NinjaCreeper.COLOR_HIGHLIGHT);
        font.drawString(484 - font.getWidth(stage3) / 2, 227, stage3, NinjaCreeper.COLOR_HIGHLIGHT);
        font.drawString(652 - font.getWidth(stage4) / 2, 227, stage4, NinjaCreeper.COLOR_HIGHLIGHT);
        glPopMatrix();
    }

    private void drawKeyControls() {
        for (ControlsButton button : controlButtons) {
            button.draw();
        }
        musicSlider.draw();
        fxSlider.draw();
    }

    private void drawLevelPanels() {
        for (LevelPanel panel : levelPanels) {
            panel.draw();
        }
    }

    private void drawGeneralButtons() {
        startGameButton.draw();
        customGameButton.draw();
    }

    private void startGameButton_mouseEvents() {
        if (startGameButton.inBounds()) {
            startGameButton.mouseWithin();
            if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()) {
                FxPlayer.playSound(AudioFx.BUTTONCLICK);
                NinjaCreeper.startGame(LevelPanel.getSelectedLevel(), null);
            }
        } else {
            startGameButton.mouseNotWithin();
        }
    }

    private void customGameButton_mouseEvents() {
        if (customGameButton.inBounds()) {
            customGameButton.mouseWithin();
            if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()) {
                FxPlayer.playSound(AudioFx.BUTTONCLICK);
                String currentPath = Paths.get("").toAbsolutePath().toString();
                JFileChooser chooser = new JFileChooser(currentPath);
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    File dir = chooser.getSelectedFile();
                    NinjaCreeper.startGame(0, dir);
                }
            }
        } else {
            customGameButton.mouseNotWithin();
        }
    }

    private void drawSpeakerButtons() {
        musicSpeaker.draw();
        fxSpeaker.draw();
    }

    private void setUpShaders() {
        shaderProgram = glCreateProgram();
        int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        BufferedReader br = new BufferedReader(new InputStreamReader(MenuScreen.class.getResourceAsStream("/ninjacreeper/menu/grayscale.fs")));
        String fragmentShaderSource = new Scanner(MenuScreen.class.getResourceAsStream("/ninjacreeper/menu/grayscale.fs"), "UTF-8").useDelimiter("\\A").next();

        glShaderSource(fragmentShader, fragmentShaderSource);
        glCompileShader(fragmentShader);
        glAttachShader(shaderProgram, fragmentShader);
        glLinkProgram(shaderProgram);
        glValidateProgram(shaderProgram);
    }
}

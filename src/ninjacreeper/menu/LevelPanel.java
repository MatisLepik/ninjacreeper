package ninjacreeper.menu;

import ninjacreeper.game.AudioFx;
import ninjacreeper.game.FxPlayer;
import ninjacreeper.game.NinjaCreeper;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.Color;

/**
 *
 * @author Matis
 */
public class LevelPanel extends GUIComponent {

    private static int selectedLevel = 1;
    private final int level;
    private boolean isHighlighted;

    public LevelPanel(double x, double y, int level) {
        super(x, y, 140, 105, true);
        this.level = level;
        setTextureX_max(840);
        setTextureX_min(700);
        setTextureY_max(105);
        setTextureY_min(0);
        font = NinjaCreeper.FONT_SMALL;
    }

    @Override
    public void draw() {
        drawScore();
        if (level == selectedLevel || isHighlighted) {
            super.draw();
        }
    }

    @Override
    public void mousePressed() {
        selectedLevel = level;
        FxPlayer.playSound(AudioFx.BUTTONCLICK);
        System.out.println("Clicked this" + selectedLevel);
    }

    @Override
    public void mouseWithin() {
        isHighlighted = true;
        if (!mouseOvered) {
            FxPlayer.playSound(AudioFx.MOUSEOVER);
            mouseOvered = true;
        }
    }

    @Override
    public void mouseNotWithin() {
        mouseOvered = false;
        isHighlighted = false;
    }

    public int getLevel() {
        return level;
    }

    public static int getSelectedLevel() {
        return selectedLevel;
    }

    private void drawScore() {
        glPushMatrix();
        new Color(0f, 0f, 0f, 0.5f).bind();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glTranslated(getX(), getY(), 0);
        glBegin(GL_QUADS);

        glVertex2d(0, 0); // Top left
        glVertex2d(0, 25); // Bottom left
        glVertex2d(getWidth(), 25); // Bottom right
        glVertex2d(getWidth(), 0); // Top right

        glEnd();

        Color scoreCol = new Color(NinjaCreeper.COLOR_HIGHLIGHT.r, NinjaCreeper.COLOR_HIGHLIGHT.g, NinjaCreeper.COLOR_HIGHLIGHT.b, 1f);
        String scoreText;
        double score = NinjaCreeper.options[level + 8];
        if (score > 0) {
            scoreText = "BEST: " + score + " SEC";
        } else {
            scoreText = "NO BEST TIME YET.";
        }

        font.drawString(((int) getWidth() / 2) - (font.getWidth(scoreText) / 2), 5, scoreText, scoreCol);
        NinjaCreeper.TEX_SPRITESHEET.bind();
        glPopMatrix();
        Color.white.bind();
    }
}

uniform sampler2D image;

void main() {
        vec3 texel = texture2D(image, gl_TexCoord[0].xy).rgb;
        gl_FragColor = vec4(texel.x,texel.y,texel.z, 1.0);

        gl_FragColor = max(texel.r,max(texel.g,texel.b));
}

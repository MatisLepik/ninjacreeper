package ninjacreeper.menu;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import ninjacreeper.game.AudioFx;
import ninjacreeper.game.FxPlayer;
import ninjacreeper.game.NinjaCreeper;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Matis
 */
public class VolumeSlider extends GUIComponent {

    private final VolumeType volumeType;
    private final String FxString = "FX VOLUME";
    private final String MusicString = "MUSIC VOLUME";
    private boolean isMouseWithin = false;

    public VolumeSlider(float x, float y, float width, float height, VolumeType volumeType) {
        super(x, y, width, height, false);
        this.volumeType = volumeType;
        font = NinjaCreeper.FONT_SMALL;
    }

    @Override
    public void draw() {
        String textToDraw = null;
        switch (volumeType) {
            case FX:
                drawSlider((int)NinjaCreeper.options[NinjaCreeper.OPT_FX_VOLUME]);
                textToDraw = FxString;
                break;
            case MUSIC:
                drawSlider((int)NinjaCreeper.options[NinjaCreeper.OPT_MUSIC_VOLUME]);
                textToDraw = MusicString;
                break;
        }
        if (!isMouseWithin) {
            glPushMatrix();
            glEnable(GL_TEXTURE_2D);
            glEnable(GL_BLEND);
            glTranslated(getX(), getY(), 0);

            font.drawString(50 - font.getWidth(textToDraw) / 2, (float) getHeight() / 2f - font.getHeight(textToDraw) / 2 - 2, textToDraw, color);
            NinjaCreeper.TEX_SPRITESHEET.bind();
            glPopMatrix();
        }
    }

    @Override
    public void mouseWithin() {
        if (!mouseOvered) {
            FxPlayer.playSound(AudioFx.MOUSEOVER);
            mouseOvered = true;
        }
        isMouseWithin = true;
    }

    @Override
    public void mouseNotWithin() {
        mouseOvered = false;
        isMouseWithin = false;
    }

    public void drawSlider(int volume) {
        glDisable(GL_TEXTURE_2D);
        glPushMatrix();
        glTranslated(getX(), getY(), 0);
        DoubleBuffer vertexData = BufferUtils.createDoubleBuffer(8);
        vertexData.put(new double[]{0, 2, 0, getHeight() - 2, volume, getHeight() - 2, volume, 2});
        FloatBuffer colorData = BufferUtils.createFloatBuffer(12);
        colorData.put(new float[]{NinjaCreeper.COLOR_HIGHLIGHT.r, NinjaCreeper.COLOR_HIGHLIGHT.g, NinjaCreeper.COLOR_HIGHLIGHT.b, NinjaCreeper.COLOR_HIGHLIGHT.r, NinjaCreeper.COLOR_HIGHLIGHT.g, NinjaCreeper.COLOR_HIGHLIGHT.b, NinjaCreeper.COLOR_HIGHLIGHT.r, NinjaCreeper.COLOR_HIGHLIGHT.g, NinjaCreeper.COLOR_HIGHLIGHT.b, NinjaCreeper.COLOR_HIGHLIGHT.r, NinjaCreeper.COLOR_HIGHLIGHT.g, NinjaCreeper.COLOR_HIGHLIGHT.b});

        colorData.flip();
        vertexData.flip();

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);

        glVertexPointer(2, 0, vertexData);
        glColorPointer(3, 0, colorData);
        glDrawArrays(GL_QUADS, 0, 4);
        glPopMatrix();
        glDisableClientState(GL_COLOR_ARRAY);
        glDisableClientState(GL_VERTEX_ARRAY);
    }

    public double getDistanceFromX() {
        if (NinjaCreeper.getMouseY() > getY() && NinjaCreeper.getMouseY() < getY() + getHeight()) {
            return NinjaCreeper.getMouseX() - getX();
        }
        return -1;
    }

    public void setVolume(int newVol) {
        switch (volumeType) {
            case FX:
                NinjaCreeper.options[NinjaCreeper.OPT_FX_VOLUME] = newVol;
                FxPlayer.refreshFxVolume();
                break;
            case MUSIC:
                NinjaCreeper.options[NinjaCreeper.OPT_MUSIC_VOLUME] = newVol;
                MusicPlayer.refreshMusicVolume();
                break;
        }
    }

    @Override
    public void mousePressed() {
        FxPlayer.playSound(AudioFx.BUTTONCLICK);
    }

}

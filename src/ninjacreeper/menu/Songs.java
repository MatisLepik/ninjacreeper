
package ninjacreeper.menu;

/**
 *
 * @author Matis
 */
public enum Songs {
    FRACTION("Selulance - Fraction (Original Mix).ogg"), FUNKBLASTER("Selulance - Funkblaster v14.ogg"), ICESKATING("Selulance - Ice Skating 32.ogg"), PYSEN("Selulance - Pysen (Second Mix).ogg");
    
    private final String RESOURCEFOLDER = "/ninjacreeper/resources/audio/music/";
    private final String location;
    
    private Songs(String location) {
        this.location = location;
    }
    
    
    public String getLocation() {
        return RESOURCEFOLDER+location;
    }
}
